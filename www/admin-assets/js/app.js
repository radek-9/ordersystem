
$(document).ready(function () {

	$.nette.init();

	bindGlobalJs($(document));

	$.nette.ext("snippets").after(function ($el) {
		bindGlobalJs($el);
	});

	$.nette.ext('spinner', {
		start: function () {
			$("#ajax-spinner").slideDown(100);
		},
		complete: function () {
			$("#ajax-spinner").slideUp(100);
		}
	});
});

function bindGlobalJs($el) {

	$el.find(".datagrid-inline-edit-trigger").on("click", function (e) {
		e.stopPropagation();

		$(this).netteAjax(e);
	});

	$el.find('select').select2({
		theme: 'bootstrap',
		dropdownAutoWidth : true,
		width: 'auto'
	});

	$el.find(".alert").each(function (i, el) {

		var icon = "info";

		if ($(el).hasClass("alert-success")) {
			icon = "success";

		} else if ($(el).hasClass("alert-danger")) {
			icon = "error";

		} else if ($(el).hasClass("alert-warning")) {
			icon = "warning";
		}

		$.toast({
			text: $(el).html(),
			showHideTransition: 'slide',
			position: 'top-right',
			icon: icon
		})
	});

	$el.find("[data-confirm]").on("click", function () {
		return confirm($(this).data("confirm"));
	});

	$el.find('input[data-dateinput-type]').dateinput({
		datetime: {
			dateFormat: 'd.m.yy',
			timeFormat: 'H:mm',
			options: { // options for type=datetime
				changeYear: true
			}
		},
		'datetime-local': {
			dateFormat: 'd.m.yy',
			timeFormat: 'H:mm'
		},
		date: {
			dateFormat: 'd.m.yy'
		},
		month: {
			dateFormat: 'MM yy'
		},
		week: {
			dateFormat: "w. 'week of' yy"
		},
		time: {
			timeFormat: 'H:mm'
		},
		options: { // global options
			closeText: "Close"
		}
	});
}

/**
 * Prints.
 * @param idName Optional id nad name of an iframe. If omitted, whole page will be printed.
 * @return Allways true.
 */
printTarget = function(idName) {
	if (typeof(idName) === typeof(undefined)) {	// main window
		window.focus();
		window.print();
	} else {	// iframe
		window.frames[idName].focus();
		window.frames[idName].print();
	}

	return true;
};

/**
 * Overrides default ctrl + p (cmd + p).
 * Focuses iframe and selects iframe div to print the iframe content.
 * @param idName Optional id nad name of an iframe. If omitted, whole page will be printed.
 */
overrideCtrlP = function(idName, beforeCB) {
	getWin = function() {
		var win;

		if (typeof(idName) === typeof(undefined)) {	// main window
			win = window;
		} else {	// iframe
			win = window.frames[idName];

			if (win.contentWindow) {
				win = win.contentWindow;
			}
		}

		return win;
	};

	///////////////////////////////////////////////////////////////////////////// warning //
	////////////////////////////////////////////////////////////////////////////////////////
	beforePrint = function() {
		var win = getWin();

		if (!$(win).is(':focus')) {
			alert('Pro správnou funkci tiskněte pomocí tlačítka tisk');
		}
	};

	///////////////////////////////////////////////////////////////////////// text select //
	////////////////////////////////////////////////////////////////////////////////////////
	selectText = function(win) {
		var doc = win.document
			, text = doc.getElementsByTagName("DIV")[0]
			, range, selection;

		if (doc.body.createTextRange) {
			range = doc.body.createTextRange();

			range.moveToElementText(text);
			range.select();
		} else if (win.getSelection) {
			selection = win.getSelection();
			range = doc.createRange();

			range.selectNodeContents(text);
			selection.removeAllRanges();
			selection.addRange(range);
		}
	};

	deselectText = function(win) {
		var doc = win.document
			, range, selection;

		if (doc.body.createTextRange) {
			range = doc.body.createTextRange();

			range.select();
		} else if (win.getSelection) {
			selection = win.getSelection();

			selection.removeAllRanges();
		}
	};

	///////////////////////////////////////////////////////////////////// focus and print //
	////////////////////////////////////////////////////////////////////////////////////////
	isCtrl = function(ev) {
		var isMac = navigator.platform.toUpperCase().indexOf('MAC') >= 0;

		return (isMac && ev.metaKey) || (!isMac && ev.ctrlKey);
	};

	ctrlDown = function(ev) {	// in chrome only focused and particulary selected frame can be printed
		var win = getWin();
		var key = ev.which || ev.keyCode;

		if (isCtrl(ev)) {
			win.focus();

			selectText(win);

			if (beforeCB !== undefined) {
				eval('win.' + beforeCB + ' ? win.' + beforeCB + '() : null');
			}
		}
	};

	fullLoad = function(ev) {	// in chrome only focused and particulary selected frame can be printed
		var win = getWin();
		var key = ev.which || ev.keyCode;

		win.focus();
		selectText(win);

		if (beforeCB !== undefined) {
			eval('win.' + beforeCB + ' ? win.' + beforeCB + '() : null');
		}
	};

	ctrlUp = function(ev) {	// in chrome only focused and particulary selected frame can be printed
		var win = getWin();
		var key = ev.which || ev.keyCode;

		deselectText(win);
	};

	ctrlP = function(ev) {	// not supported by chrome
		var win = getWin();
		var key = ev.which || ev.keyCode;

		if (key == 112 && isCtrl(ev)) {	// p + ctrl
			win.print();

			ev.preventDefault();
			ev.stopPropagation();
		}
	};

	//////////////////////////////////////////////////////////////////////////////// bind //
	////////////////////////////////////////////////////////////////////////////////////////
	if (window.matchMedia) {
		var mediaQueryList = window.matchMedia('print');

		mediaQueryList.addListener(function(mql) {
			if (mql.matches) {
				beforePrint();
			}
		});
	}

	window.onbeforeprint = beforePrint;

	$(window).on('keydown', ctrlDown);
	$(window).on('keypress', ctrlP);
	$(window).on('load', fullLoad);

	if (typeof(idName) !== typeof(undefined)) {	// iframe
		$(window.frames[idName]).on('keydown', ctrlDown);
		$(window.frames[idName]).on('keypress', ctrlP);
		$(window.frames[idName]).on('load', fullLoad);
	}
};

