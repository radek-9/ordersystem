<?php

declare(strict_types=1);

define("UPLOAD_DIR", "upload");
define("UPLOAD_PATH", __DIR__ . DIRECTORY_SEPARATOR . UPLOAD_DIR);

define("EMAIL_PATH", __DIR__ . "/../app/templates/snippets/emails/");
define("CSS_PATH", __DIR__ . "/assets/css/");

require __DIR__ . '/../vendor/autoload.php';

App\Bootstrap::boot()
	->createContainer()
	->getByType(Nette\Application\Application::class)
	->run();
