<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\ISearchForm;

class BasePresenter extends \App\Presenters\BasePresenter {

    /**
     * @param ISearchForm $factory
     * @return \App\AdminModule\Components\SearchForm
     */
    protected function createComponentSearchForm(ISearchForm $factory) {

        return $factory->create();
    }

}