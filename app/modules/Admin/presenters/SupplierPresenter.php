<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\ISupplierForm;
use App\AdminModule\Components\ISupplierGrid;
use App\Entities\Supplier;
use Nette\Application\UI\Form;

class SupplierPresenter extends BaseSecurePresenter {

    /** @var \App\Services\Supplier @autowire */
    protected $supplierService;

    public function actionDefault() {

    }

    /**
     * @param null $id
     * @throws \Nette\Application\BadRequestException
     */
    public function actionEdit($id = NULL) {

        if (!$this->user->allowed('supplier.add')) {
            $this->error();
        }

        if ($id) {

            if (!$this->user->allowed('supplier.edit')) {
                $this->error();
            }

            /** @var Supplier $supplier */
            $supplier = $this->em->getRepository(Supplier::class)->find($id);

            if (!$supplier) {
                $this->error();
            }

            $this["supplierForm"]->setSupplier($supplier);

            $this->template->row = $supplier;
        }
    }

    /**
     * @param ISupplierForm $factory
     * @return \App\AdminModule\Components\SupplierForm
     */
    protected function createComponentSupplierForm(ISupplierForm $factory) {
        $control = $factory->create();
        $control["form"]->onSuccess[] = function (Form $form) {
            $values = $form->getValues();

            $this->flashMessage("Uloženo", "success");

            if ($values->id) {
                $this->redirect("this");
            } else {
                $this->redirect("Supplier:");
            }
        };

        return $control;
    }

    /**
     * @param ISupplierGrid $factory
     * @return \App\AdminModule\Components\SupplierGrid
     */
    protected function createComponentSupplierGrid(ISupplierGrid $factory) {
        return $factory->create();
    }

    /**
     * @param $id
     * @throws \Nette\Application\AbortException
     * @throws \Nette\Application\BadRequestException
     */
    public function handleDelete($id) {

        if (!$this->user->allowed("supplier.delete")) {
            $this->error();
        }

        if ($this->supplierService->deleteSupplier($id)) {
            $this->flashMessage("Dodavatel smazán", "success");

        } else {
            $this->flashMessage("Dodavatele nelze smazat", "danger");
        }

        $this->redirect("this");
    }
}