<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\IProfileForm;

class ProfilePresenter extends BaseSecurePresenter {

    public function actionDefault() {
        $this->redirect("setting");
    }

    /**
     * @param IProfileForm $factory
     * @return \App\AdminModule\Components\ProfileForm
     */
    protected function createComponentProfileForm(IProfileForm $factory) {
        $control = $factory->create();
        $control["form"]->onSuccess[] = function () {
            $this->flashMessage("Uloženo", "success");
            $this->redirect("this");
        };
        return $control;
    }

    public function actionSetting() {
        $this["profileForm"]->setUser($this->user->identity);
    }
}