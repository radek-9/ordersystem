<?php

namespace App\AdminModule\Presenters;


class BaseSecurePresenter extends BasePresenter {

    /**
     * @throws \Nette\Application\AbortException
     */
	public function startup() {

	    parent::startup();

        // uživatel není přihlášen
        if (!$this->user->isLoggedIn()) {
            $this->redirect(":Admin:Sign:in");
        }

        // uživatel nemá přístup do administrace
        if (!$this->user->identity->aclRole->isAdmin || $this->user->identity->deleted) {
            $this->flashMessage("Nemáte přístup do administrace", "danger");
            $this->user->logout(TRUE);
            $this->redirect(":Admin:Sign:in");
        }
	}

    /**
     * @throws \Exception
     */
    public function beforeRender() {

        parent::beforeRender();

    }
}