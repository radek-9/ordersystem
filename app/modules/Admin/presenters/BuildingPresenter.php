<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\IBuildingForm;
use App\AdminModule\Components\IBuildingGrid;
use App\Entities\Building;
use Nette\Application\UI\Form;

class BuildingPresenter extends BaseSecurePresenter {

    /** @var \App\Services\Building @autowire */
    protected $buildingService;

    public function actionDefault() {

    }

    /**
     * @param null $id
     * @throws \Nette\Application\BadRequestException
     */
    public function actionEdit($id = NULL) {

        if (!$this->user->allowed('building.edit')) {
            $this->error();
        }

        if ($id) {
            /** @var Building $building */
            $building = $this->em->getRepository(Building::class)->find($id);

            $isManagerBuilding = FALSE;
            if ($building->managers) {
                foreach ($building->managers as $manager) {
                    if ($manager->manager->id === $this->user->identity->getId() || $this->user->isAdmin()) {
                        $isManagerBuilding = TRUE;
                    }
                }
            }

            if (!$building || !$isManagerBuilding) {
                $this->error();
            }

            $this["buildingForm"]->setBuilding($building);

            $this->template->row = $building;
        }
    }

    /**
     * @param IBuildingForm $factory
     * @return \App\AdminModule\Components\BuildingForm
     */
    protected function createComponentBuildingForm(IBuildingForm $factory) {
        $control = $factory->create();
        $control["form"]->onSuccess[] = function (Form $form) {
            $values = $form->getValues();

            $this->flashMessage("Uloženo", "success");

            if ($values->id) {
                $this->redirect("this");
            } else {
                $this->redirect("Building:");
            }
        };

        return $control;
    }

    /**
     * @param IBuildingGrid $factory
     * @return \App\AdminModule\Components\BuildingGrid
     */
    protected function createComponentBuildingGrid(IBuildingGrid $factory) {
        return $factory->create();
    }

    /**
     * @param $id
     * @throws \Nette\Application\AbortException
     * @throws \Nette\Application\BadRequestException
     */
    public function handleDelete($id) {

        if (!$this->user->allowed("building.delete")) {
            $this->error();
        }

        if ($this->buildingService->deleteBuilding($id)) {
            $this->flashMessage("Stavba smazána", "success");

        } else {
            $this->flashMessage("Stavbu nelze smazat", "danger");
        }

        $this->redirect("this");
    }
}