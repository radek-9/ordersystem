<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\ILoginForm;

class SignPresenter extends BasePresenter {

	/**
	 * @param ILoginForm $factory
	 * @return \App\AdminModule\Components\LoginForm
	 */
	protected function createComponentLoginForm(ILoginForm $factory) {
		return $factory->create();
	}

    /**
     * @throws \Nette\Application\AbortException
     */
	public function actionOut() {
		$this->user->logout(TRUE);
		$this->redirect("in");
	}
}