<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\IOrderForm;
use App\AdminModule\Components\IOrderGrid;
use App\Entities\Order;
use App\Facades\Mail;
use Mpdf\Mpdf;
use Nette\Application\UI\Form;

class OrderPresenter extends BaseSecurePresenter {

    /** @var \App\Services\Order @autowire */
    protected $orderService;

    /** @var Mail @autowire */
    protected $mailFacade;

    /**
     * @param null $search
     */
    public function actionDefault($search = NULL) {

        $this->template->search = $search;
    }

    /**
     * @param null $id
     * @throws \Nette\Application\BadRequestException
     */
    public function actionEdit($id = NULL) {

        if (!$this->user->allowed('order.edit')) {
            $this->error();
        }

        if ($id) {
            /** @var Order $order */
            $order = $this->em->getRepository(Order::class)->find($id);

            if (!$order) {
                $this->error();
            }

            $this["orderForm"]->setOrder($order);

            $this->template->row = $order;
        }
    }

    /**
     * @param $id
     * @throws \Mpdf\MpdfException
     * @throws \Nette\Application\BadRequestException
     */
    public function actionPdf($id) {

        if (!$this->user->allowed('order.pdf')) {
            $this->error();
        }

        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)->find($id);

        if (!$order) {
            $this->error();
        }

        $this->template->row = $order;

        $this->getContentPdfToView($order);
    }

    /**
     * @param IOrderForm $factory
     * @return \App\AdminModule\Components\OrderForm
     */
    protected function createComponentOrderForm(IOrderForm $factory) {
        $control = $factory->create();
        $control["form"]->onSuccess[] = function (Form $form) {
            $values = $form->getValues();

            $this->flashMessage("Uloženo", "success");

            if ($values->id) {
                $this->redirect("this");
            } else {
                $this->redirect("Order:");
            }
        };

        return $control;
    }

    /**
     * @param IOrderGrid $factory
     * @return \App\AdminModule\Components\OrderGrid
     */
    protected function createComponentOrderGrid(IOrderGrid $factory) {
        return $factory->create();
    }

    /**
     * @param $id
     * @throws \Nette\Application\AbortException
     * @throws \Nette\Application\BadRequestException
     */
    public function handleCancelled($id) {

        if (!$this->user->allowed("order.cancelled")) {
            $this->error();
        }

        if ($this->orderService->cancelledOrder($id)) {
            $this->flashMessage("Objednávka stornována", "success");

        } else {
            $this->flashMessage("Objednávku nelze stornovat", "danger");
        }

        $this->redirect("this");
    }

    /**
     * @param $id
     * @throws \Mpdf\MpdfException
     * @throws \Nette\Application\AbortException
     * @throws \Nette\Application\BadRequestException
     */
    public function handleSendOrder($id) {

        if (!$this->user->allowed("order.pdf")) {
            $this->error();
        }

        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)->find($id);

        if (!$order) {
            $this->error();
        }

        $pdfName = $order->building->id . '_' . $order->building->name . '_' . $order->manager->lastName . '_' . $order->supplier->company . '_' . $order->created->format("d-m-Y-H-i") . '.pdf';

        $this->mailFacade->sendOrderEmail($order, $pdfName, $this->getContentPdfToMail($order));

        $order->state = Order::SHIPPED;
        $this->em->flush($order);

        $this->flashMessage("Objednávka odeslána", "success");

        $this->redirect("this");
    }

    /**
     * @param $documentId
     * @throws \Nette\Application\AbortException
     */
    public function handleDeleteDocument($documentId) {

        if ($this->orderService->deleteOrderDocument($documentId)) {
            $this->flashMessage("Dokument odstraněn", "success");

        } else {
            $this->flashMessage("Dokument se nepodařilo odstranit", "danger");
        }

        $this->redirect("this");
    }

    /**
     * @param Order $order
     * @return string
     * @throws \Mpdf\MpdfException
     */
    private function getContentPdfToView(Order $order) {

        // Vytváření šablony pro PDF.
        /** @var \Nette\Bridges\ApplicationLatte\Template $template */
        $template = $this->createTemplate();
        $template->setFile(__DIR__ . '/../templates/Order/pdfTemplate.latte');
        $template->order = $order;

        // Render PDF.
        $mPdf = new Mpdf();

        $mPdf->WriteHTML($template);

        // Return PDF.
        $mPdf->Output();
        //$this->terminate();
    }

    /**
     * @param Order $order
     * @return string
     * @throws \Mpdf\MpdfException
     */
    private function getContentPdfToMail(Order $order) {

        /** @var \Nette\Bridges\ApplicationLatte\Template $template */
        $template = $this->createTemplate();
        $template->setFile(__DIR__ . '/../templates/Order/pdfTemplate.latte');
        $template->order = $order;

        $mPdf = new Mpdf();

        $mPdf->WriteHTML($template);

        $content = $mPdf->Output('', 'S'); // Saving pdf to attach to email

        return $content;
    }
}