<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\ICustomerForm;
use App\AdminModule\Components\ICustomerGrid;
use App\AdminModule\Components\IUserForm;
use App\AdminModule\Components\IUserGrid;
use App\Entities\User;
use App\Facades\Mail;
use Nette\Application\UI\Form;

class UserPresenter extends BaseSecurePresenter {

    /** @var \App\Services\Building @autowire */
    protected $buildingService;

    /** @var Mail @autowire */
    public $mailFacade;

    /**
     * @throws \Nette\Application\BadRequestException
     */
	public function actionUser() {

		if (!$this->user->allowed('user.user')) {
			$this->error();
		}
	}

    /**
     * @throws \Nette\Application\BadRequestException
     */
    public function actionCustomer() {

        if (!$this->user->allowed('customer.customer')) {
            $this->error();
        }
    }

    /**
     * @param null $id
     * @throws \Nette\Application\BadRequestException
     */
	public function actionEditUser($id = NULL) {

        if (!$this->user->allowed('user.user')) {
            $this->error();
        }

		if ($id) {
			/** @var User $user */
			$user = $this->em->getRepository(User::class)->find($id);

			if (!$user) {
				$this->error();
			}

			$this["userForm"]->setUser($user);

			$this->template->row = $user;
			$this->template->buildings = $this->buildingService->getBuildings($id);
		}
	}

    /**
     * @param null $id
     * @throws \Nette\Application\BadRequestException
     */
    public function actionEditCustomer($id = NULL) {

        if (!$this->user->allowed('customer.customer')) {
            $this->error();
        }

        if ($id) {
            /** @var User $user */
            $user = $this->em->getRepository(User::class)->find($id);

            if (!$user) {
                $this->error();
            }

            $this["customerForm"]->setCustomer($user);

            $this->template->row = $user;
        }
    }

	/**
	 * @param IUserForm $factory
	 * @return \App\AdminModule\Components\UserForm
	 */
	protected function createComponentUserForm(IUserForm $factory) {
		$control = $factory->create();
		$control["form"]->onSuccess[] = function (Form $form) {
			$values = $form->getValues();

			$this->flashMessage("Uloženo", "success");

			if ($values->id) {
				$this->redirect("this");
			} else {
				$this->redirect("User:user");
			}
		};

		return $control;
	}

    /**
     * @param IUserGrid $factory
     * @return \App\AdminModule\Components\UserGrid
     */
    protected function createComponentUserGrid(IUserGrid $factory) {
        return $factory->create();
    }

    /**
     * @param ICustomerForm $factory
     * @return \App\AdminModule\Components\CustomerForm
     */
    protected function createComponentCustomerForm(ICustomerForm $factory) {
        $control = $factory->create();
        $control["form"]->onSuccess[] = function (Form $form) {
            $values = $form->getValues();

            $this->flashMessage("Uloženo", "success");

            if ($values->id) {
                $this->redirect("this");
            } else {
                $this->redirect("User:customer");
            }
        };

        return $control;
    }

    /**
     * @param ICustomerGrid $factory
     * @return \App\AdminModule\Components\CustomerGrid
     */
    protected function createComponentCustomerGrid(ICustomerGrid $factory) {
        return $factory->create();
    }
}