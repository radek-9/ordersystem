<?php

namespace App\AdminModule\Components;

use App\Entities\Building;
use Nette\Application\UI\Form;
use Nette\Utils\Html;

interface IBuildingGrid {

    /** @return BuildingGrid */
    function create();
}

class BuildingGrid extends \App\Components\BaseControl {

    /** @var \App\Services\User @autowire */
    protected $userService;

    /**
     * @return Form
     */
    protected function createComponentGrid() {
        $grid = new \App\Components\DataGrid();

        $grid->addColumnText("id", "Id")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("name", "Název")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("street", "Ulice")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("city", "Město")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("dateFrom", "Datum od")
            ->setRenderer(function (Building $building) {
            return $building->dateFrom->format("d.m.Y");
        });

        $grid->addColumnText("dateTo", "Datum do")
            ->setRenderer(function (Building $building) {
                return $building->dateTo->format("d.m.Y");
            });

        $grid->addColumnText("manager", "Manager")
            ->setRenderer(function(Building $building) {
                $managers = [];
                if ($building->managers) {
                    foreach ($building->managers as $manager) {
                        $managers[$manager->manager->id] = $manager->manager->getFullName();
                    }
                    return implode(", ", $managers);
                } else {
                    return NULL;
                }
            });

        $grid->addColumnText("state", "Stav")
            ->setRenderer(function (Building $building) {
                return Building::getStateName($building->state);
            });

        if ($this->user->allowed("building.edit")) {
            $grid->addAction("edit", "edit")
                ->setRenderer(function (Building $building) {
                    $isManagerBuilding = FALSE;
                    if ($building->managers) {
                        foreach ($building->managers as $manager) {
                            if ($manager->manager->id === $this->user->identity->getId()) {
                                $isManagerBuilding = TRUE;
                            }
                        }
                    }
                    if ($isManagerBuilding || $this->user->isAdmin()) {
                        return Html::el("a", [
                            "href" => $this->presenter->link("edit", $building->id),
                            "class" => "btn btn-success btn-xs",
                        ])->setHtml('<i class="fa fa-pencil"></i>');
                    } else {
                        return NULL;
                    }
                });
        }

        if ($this->user->allowed("building.delete")) {
            $grid->addAction("delete", "delete")
                ->setRenderer(function (Building $building) {
                    return Html::el("a", [
                        "href" => $this->presenter->link("delete!", $building->id),
                        "class" => "btn btn-danger btn-xs",
                        "data-confirm" => "Opravdu smazat?",
                    ])->setHtml('<i class="fa fa-trash"></i>');
                });
        }

        $grid->setDataSource(
            $this->em->createQueryBuilder()
                ->select("e")
                ->from(Building::class, "e")
                ->andWhere("e.deleted = 0")
                ->orderBy("e.id", "DESC")
        );

        // filtr
        // -------------------------------------------------------------------------------------------------------------

        foreach (["name", "street", "city"] as $column) {
            $grid->addFilterText($column, $column)
                ->setCondition(function ($qb, $value) use ($column) {
                    $qb->andWhere("e.$column LIKE :$column")
                        ->setParameter($column, "%" . $value . "%");
                });
        };

        $grid->addFilterDateRange("dateFrom", "Datum od")
            ->addAttribute("data-dateinput-type", "date")
            ->setCondition(function ($qb, $value) {

                if ($value["from"]) {
                    $value["from"] = (new \DateTime($value["from"]))->modify("00:00:00")->format("Y-m-d");
                    $qb->andWhere("e.dateFrom >= :dateFrom")
                        ->setParameter("dateFrom", $value["from"]);
                }

                if ($value["to"]) {
                    $value["to"] = (new \DateTime($value["to"]))->modify("23:59:59")->format("Y-m-d");
                    $qb->andWhere("e.dateFrom <= :dateTo")
                        ->setParameter("dateTo", $value["to"]);
                }
            });

        $grid->addFilterDateRange("dateTo", "Datum do")
            ->addAttribute("data-dateinput-type", "date")
            ->setCondition(function ($qb, $value) {

                if ($value["from"]) {
                    $value["from"] = (new \DateTime($value["from"]))->modify("00:00:00")->format("Y-m-d");
                    $qb->andWhere("e.dateFrom >= :dateFrom")
                        ->setParameter("dateFrom", $value["from"]);
                }

                if ($value["to"]) {
                    $value["to"] = (new \DateTime($value["to"]))->modify("23:59:59")->format("Y-m-d");
                    $qb->andWhere("e.dateFrom <= :dateTo")
                        ->setParameter("dateTo", $value["to"]);
                }
            });

        $grid->addFilterMultiSelect("manager", "Manager", $this->userService->getUserPairs(FALSE, FALSE, TRUE))
            ->setCondition(function ($qb, $value) {
                $qb->join("e.managers", "m")
                    ->andWhere("m.manager IN (:manager)")
                    ->setParameter("manager", $value[0]);
            });

        $grid->addFilterSelect("state", "State", [NULL => "---"] + Building::getStatePairs())
            ->setCondition(function ($qb, $value) {
                $qb->andWhere("e.state = :state")
                    ->setParameter("state", $value);
            });

        return $grid;
    }

}
