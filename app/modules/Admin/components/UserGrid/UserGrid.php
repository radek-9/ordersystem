<?php

namespace App\AdminModule\Components;

use App\Entities\AclRole;
use App\Entities\User;
use Nette\Application\UI\Form;
use Nette\Utils\Html;

interface IUserGrid {

    /** @return UserGrid */
    function create();
}

class UserGrid extends \App\Components\BaseControl {

    /**
     * @return Form
     */
    protected function createComponentGrid() {
        $grid = new \App\Components\DataGrid();

        $grid->addColumnText("avatar", "Avatar")
            ->setRenderer(function (User $user) {
                return Html::el("img", [
                    "class" => "img-responsive",
                    "src" => "/" . $user->getAvatarPath(),
                    "style" => "max-height: 30px",
                ]);
            });

        $grid->addColumnText("firstName", "Jméno")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("lastName", "Příjmení")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("email", "Email")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("phone", "Telefon")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("aclRole", "Role")
            ->setRenderer(function(User $user) {
                return ucfirst(AclRole::getRoleName($user->aclRole->id));
            });

        $grid->addColumnNumber("active", "Aktivovaný účet")
            ->setRenderer(function (User $user) {
                return $user->active ? "Ano" : "Ne";
            });

        $grid->addColumnNumber("deleted", "Odstraněný účet")
            ->setRenderer(function (User $user) {
                return $user->deleted ? "Ano" : "Ne";
            });

        if ($this->user->allowed("user.edit")) {
            $grid->addAction("edit", "edit")
                ->setRenderer(function (User $user) {
                    return Html::el("a", [
                        "href" => $this->presenter->link("editUser", $user->id),
                        "class" => "btn btn-success btn-xs",
                    ])->setHtml('<i class="fa fa-pencil"></i>');
                });
        }

        $grid->setDataSource(
            $this->em->createQueryBuilder()
                ->select("e")
                ->from(User::class, "e")
                ->andWhere("e.aclRole != :aclRole")
                ->setParameter("aclRole", AclRole::CUSTOMER)
                ->orderBy("e.id", "DESC")
        );

        // filtr
        // -------------------------------------------------------------------------------------------------------------

        foreach (["firstName", "lastName", "email", "phone"] as $column) {
            $grid->addFilterText($column, $column)
                ->setCondition(function ($qb, $value) use ($column) {
                    $qb->andWhere("e.$column LIKE :$column")
                        ->setParameter($column, "%" . $value . "%");
                });
        };

        foreach (["active", "deleted"] as $column) {
            $grid->addFilterSelect($column, $column, [
                NULL => "---",
                1 => "Ano",
                0 => "Ne",
            ])
                ->setCondition(function ($qb, $value) use ($column) {
                    $qb->andWhere("e.$column = :$column")
                        ->setParameter($column, $value);
                });
        };

        $grid->addFilterSelect("aclRole", "aclRole", array_merge([NULL => "---"], array_map('ucfirst', AclRole::getAdminRolePairs())))
            ->setCondition(function ($qb, $value) {
                $qb->andWhere("e.aclRole = :aclRole")
                    ->setParameter("aclRole", $value);
            });

        return $grid;
    }

}
