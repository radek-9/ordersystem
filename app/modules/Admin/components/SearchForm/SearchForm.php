<?php

namespace App\AdminModule\Components;

use Nette\Application\UI\Form;

interface ISearchForm {

    /** @return SearchForm */
    function create();
}

class SearchForm extends \App\Components\BaseControl {

    /**
     * @return Form
     */
    protected function createComponentForm() {
        $form = new Form;

        $form->addProtection("Bezpečnosní token vypršel, odešlete formulář znovu.", 10 * 60);

        $form->addText("search", "")
            ->setHtmlType("search")
            ->setHtmlAttribute("placeholder", "search")
            ->setDefaultValue($this->presenter->getParameter("search"));

        $form->addSubmit('send', '')
            ->setValidationScope([])
            ->getControlPrototype()
            ->setName('button')
            ->setHtml("<i class='fa fa-search'></i>");

        $form->onSuccess[] = [$this, "formSuccess"];

        return $form;
    }

    /**
     * @param Form $form
     * @throws \Nette\Application\AbortException
     * @throws \Exception
     */
    public function formSuccess(Form $form) {

        if (!$form["send"]->isSubmittedBy()) {
            return;
        }

        $values = $form->getValues();

        $this->presenter->redirect('this', ['search' => $values->search]);
    }
}
