<?php

namespace App\AdminModule\Components;

use App\Entities\Building;
use Nette\Application\UI\Form;
use Vodacek\Forms\Controls\DateInput;

interface IBuildingForm {

	/** @return BuildingForm */
	function create();
}

class BuildingForm extends \App\Components\BaseControl {

	/** @var \App\Services\Building @autowire */
	protected $buildingService;

    /** @var \App\Services\User @autowire */
    protected $userService;

    /**
     * @param Building $building
     */
    public function setBuilding(Building $building) {

        $this["form"]->setDefaults([
            "id" => $building->id,
            "name" => $building->name,
            "street" => $building->street,
            "city" => $building->city,
            "dateFrom" => $building->dateFrom,
            "dateTo" => $building->dateTo,
            "state" => $building->state,
            "manager" => $building->manager->id ?? NULL,
        ]);

        $this->template->row = $building;
    }

	/**
	 * @return Form
	 */
    protected function createComponentForm() {
        $form = new Form;

        $form->addProtection("Bezpečnosní token vypršel, odešlete formulář znovu.", 10 * 60);

        $form->addHidden("id");

        $form->addText('name', "Název")
            ->setRequired("Pole %label je povinné");

        $form->addText('street', "Ulice")
            ->setRequired("Pole %label je povinné");

        $form->addText('city', "Město")
            ->setRequired("Pole %label je povinné");

        $form->addDate("dateFrom", "Datum od", DateInput::TYPE_DATE)
            ->setAttribute("placeholder", "dd/mm/yyyy")
            ->setRequired("Pole %label je povinné");

        $form->addDate("dateTo", "Datum do", DateInput::TYPE_DATE)
            ->setAttribute("placeholder", "dd/mm/yyyy")
            ->setRequired("Pole %label je povinné");

        $form->addMultiSelect('managers', "Manager", [NULL => "---"] + $this->userService->getUserPairs(FALSE, FALSE, TRUE));

        $form->addSelect("state", "Stav", array_map('ucfirst', Building::getStatePairs()))
            ->setRequired("Pole %label je povinné");

        $form->addSubmit('send', 'Uložit');

        $form->onSuccess[] = [$this, "formSuccess"];

        return $form;
    }

    /**
     * @param Form $form
     * @throws \Doctrine\ORM\ORMException
     */
	public function formSuccess(Form $form) {

		$values = $form->getValues();
		$this->buildingService->saveBuilding($values);
	}

}