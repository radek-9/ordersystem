<?php

namespace App\AdminModule\Components;

use App\Entities\Supplier;
use Nette\Application\UI\Form;
use Nette\Utils\Html;

interface ISupplierGrid {

    /** @return SupplierGrid */
    function create();
}

class SupplierGrid extends \App\Components\BaseControl {

    /**
     * @return Form
     */
    protected function createComponentGrid() {
        $grid = new \App\Components\DataGrid();

        $grid->addColumnText("id", "Id")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("company", "Firma")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("email", "Email")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("phone", "Telefon")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("street", "Ulice")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("city", "Město")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("zip", "PSČ")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("ino", "IČO")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("vat", "DIČ")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        if ($this->user->allowed("supplier.edit")) {
            $grid->addAction("edit", "edit")
                ->setRenderer(function (Supplier $supplier) {
                    return Html::el("a", [
                        "href" => $this->presenter->link("edit", $supplier->id),
                        "class" => "btn btn-success btn-xs",
                    ])->setHtml('<i class="fa fa-pencil"></i>');
                });
        }

        if ($this->user->allowed("supplier.delete")) {
            $grid->addAction("delete", "delete")
                ->setRenderer(function (Supplier $supplier) {
                    return Html::el("a", [
                        "href" => $this->presenter->link("delete!", $supplier->id),
                        "class" => "btn btn-danger btn-xs",
                        "data-confirm" => "Opravdu smazat?",
                    ])->setHtml('<i class="fa fa-trash"></i>');
                });
        }

        $grid->setDataSource(
            $this->em->createQueryBuilder()
                ->select("e")
                ->from(Supplier::class, "e")
                ->andWhere("e.deleted = 0")
                ->orderBy("e.id", "DESC")
        );

        // filtr
        // -------------------------------------------------------------------------------------------------------------

        foreach (["company", "email", "phone", "street", "city", "zip", "ino", "vat", "delete"] as $column) {
            $grid->addFilterText($column, $column)
                ->setCondition(function ($qb, $value) use ($column) {
                    $qb->andWhere("e.$column LIKE :$column")
                        ->setParameter($column, "%" . $value . "%");
                });
        };

        return $grid;
    }

}
