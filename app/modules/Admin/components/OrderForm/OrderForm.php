<?php

namespace App\AdminModule\Components;

use App\Entities\Order;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Forms\Controls\SubmitButton;
use Vodacek\Forms\Controls\DateInput;

interface IOrderForm {

    /** @return OrderForm */
    function create();
}

class OrderForm extends \App\Components\BaseControl {

    /** @var \App\Services\User @autowire */
    protected $userService;

    /** @var \App\Services\Supplier @autowire */
    protected $supplierService;

    /** @var \App\Services\Building @autowire */
    protected $buildingService;

    /** @var \App\Services\Order @autowire */
    protected $orderService;

    /**
     * @param Order $order
     */
    public function setOrder(Order $order) {

        if ($order->isShipped()) {
            $this["form"]["send"]->setDisabled(TRUE);
        }

        $defaults = [
            "id" => $order->id,
            "manager" => $order->manager->id,
            "supplier" => $order->supplier->id,
            "building" => $order->building->id,
            "state" => $order->state,
            "created" => $order->created,
        ];

        foreach ($order->items as $item) {
            $defaults["items"][] = [
                "id" => $item->id,
                "item" => $item->item,
            ];
        }

        $this["form"]->setDefaults($defaults);

        $this->template->row = $order;
    }

    /**
     * @return Form
     */
    protected function createComponentForm() {
        $form = new Form;

        $form->addProtection("Bezpečnosní token vypršel, odešlete formulář znovu.", 10 * 60);

        $form->addHidden("id");

        $form->addHidden('manager')
            ->setDefaultValue($this->user->identity->getId());

        $form->addText('managerName', "Manager")
            ->setDisabled(TRUE)
            ->setDefaultValue($this->user->identity->getFullName());

        $form->addSelect('supplier', "Dodavatel", [NULL => "---"] + $this->supplierService->getSuppliersPairs())
            ->setRequired("Pole %label je povinné");

        $buildingFilter = $this->user->isAdmin() ? NULL : $this->user->identity->getId();

        //$form->addSelect('building', "Stavba", [NULL => "---"] + $this->buildingService->getBuildingsPairs($buildingFilter))
        $form->addText('building', "Stavba")
            ->setRequired("Pole %label je povinné");

        $items = $form->addDynamic("items", function (Container $item) {

            $item->addHidden('id');

            $item->addText('item', "Položka")
                ->setRequired("Pole %label je povinné");

            $item->addSubmit('remove', 'Remove')
                ->setValidationScope([])
                ->getControlPrototype()
                ->setName('button')
                ->setHtml("<i class='fa fa-trash'></i>");

            $item["remove"]->onClick[] = [$this, 'removeRow'];
        }, 1);

        $items->addSubmit('add', 'Add')
            ->setValidationScope([])
            ->getControlPrototype()
            ->setName('button')
            ->setHtml("<i class='fa fa-plus'></i>");

        $items["add"]->onClick[] = [$this, 'addRow'];

        $form->addSelect("state", "Stav", array_map('ucfirst', Order::getStatePairs()))
            ->setRequired("Pole %label je povinné");

        $form->addDate("created", "Created", DateInput::TYPE_DATE)
            ->setAttribute("placeholder", "dd/mm/yyyy");

        /*$form->addUpload("document")
            ->addRule(Form::IMAGE, "document error");*/

        $form->addMultiUpload("documents", "Dokumenty")
            ->setRequired(FALSE)
            ->addRule(Form::IMAGE, "document error");

        $form->addSubmit('send', 'Uložit');

        $form->onSuccess[] = [$this, "formSuccess"];

        return $form;
    }

    /**
     * @param Form $form
     * @throws \Doctrine\ORM\ORMException
     */
    public function formSuccess(Form $form) {

        $values = $form->getValues();
        $this->orderService->saveOrder($values);
    }

}