<?php

namespace App\AdminModule\Components;

use App\Entities\Order;
use Nette\Application\UI\Form;
use Nette\Utils\Html;

interface IOrderGrid {

    /** @return OrderGrid */
    function create();
}

class OrderGrid extends \App\Components\BaseControl {

    /** @var \App\Services\User @autowire */
    protected $userService;

    /** @var \App\Services\Supplier @autowire */
    protected $supplierService;

    /** @var \App\Services\Building @autowire */
    protected $buildingService;

    /**
     * @return Form
     */
    protected function createComponentGrid() {
        $grid = new \App\Components\DataGrid();

        $grid->addColumnText("id", "Id")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("created", "Vytvořeno")
            ->setRenderer(function (Order $order) {
                return $order->created->format("d.m.Y");
            });

        $grid->addColumnText("state", "Stav")
            ->setRenderer(function (Order $order) {
                return Order::getStateName($order->state);
            });

        $grid->addColumnText("manager", "Autor objednávky")
            ->setRenderer(function (Order $order) {
                return $order->manager->getFullName();
            });

        $grid->addColumnText("supplier", "Dodavatel")
            ->setRenderer(function (Order $order) {
                return $order->supplier->company;
            });

        $grid->addColumnText("building", "Stavba")
            ->setRenderer(function (Order $order) {
                return $order->building->name;
            });

        $grid->addColumnText("managers", "Pracovníci stavby")
            ->setRenderer(function (Order $order) {
                $managers = [];
                if ($order->building->managers) {
                    foreach ($order->building->managers as $manager) {
                        $managers[] = $manager->manager->getFullName();
                    }
                    return implode(", ", $managers);
                } else {
                    return NULL;
                }
            });

        $grid->addColumnText("items", "Položky")
            ->setRenderer(function (Order $order) {
                $items = [];
                if ($order->items) {
                    foreach ($order->items as $item) {
                        $items[] = $item->item;
                    }
                    return implode(", ", $items);
                } else {
                    return NULL;
                }
            });

        if ($this->user->allowed("order.edit")) {
            $grid->addAction("edit", "edit")
                ->setRenderer(function (Order $order) {
                    return Html::el("a", [
                        "href" => $this->presenter->link("edit", $order->id),
                        "class" => "btn btn-success btn-xs",
                    ])->setHtml('<i class="fa fa-pencil"></i>');
                });
        }

        /*if ($this->user->allowed("order.pdf")) {
            $grid->addAction("pdf", "pdf")
                ->setRenderer(function (Order $order) {
                    return Html::el("a", [
                        "href" => $this->presenter->link("pdf", $order->id),
                        "class" => "btn btn-info btn-xs",
                        "target" => "_blank",
                        "rel" => "noopener noreferrer",
                    ])->setHtml('<i class="fa fa-file-pdf-o"></i>');
                });
        }*/

        if ($this->user->allowed("order.cancelled")) {
            $grid->addAction("cancelled", "cancelled")
                ->setRenderer(function (Order $order) {
                    return Html::el("a", [
                        "href" => $this->presenter->link("cancelled!", $order->id),
                        "class" => "btn btn-danger btn-xs",
                        "data-confirm" => "Opravdu stornovat?",
                    ])->setHtml('<i class="fa fa-ban"></i>');
                });
        }


        $dataSource = $this->em->createQueryBuilder()
            ->select("e")
            ->from(Order::class, "e")
            ->join("e.building", "b")
            ->join("e.supplier", "s")
            ->leftJoin("b.managers", "m")
            ->join("m.manager", "u")
            ->leftJoin("e.items", "i")
            ->orderBy("e.id", "DESC");

        if (!$this->user->isAdmin()) {
            $dataSource->andWhere("m.manager = :manager")
                ->setParameter("manager", $this->user->identity->getId());
        }

        if ($search = $this->presenter->getParameter("search")) {
            $dataSource->andWhere("
                u.firstName LIKE :search
                OR
                u.lastName LIKE :search
                OR
                s.company LIKE :search
                OR
                b.name LIKE :search
                OR
                i.item LIKE :search
            ")
                ->setParameter("search", "%" . $search . "%");
        }

        $grid->setDataSource($dataSource);

        // filtr
        // -------------------------------------------------------------------------------------------------------------

        $grid->addFilterDateRange("created", "Vytvořeno")
            ->addAttribute("data-dateinput-type", "date")
            ->setCondition(function ($qb, $value) {

                if ($value["from"]) {
                    $value["from"] = (new \DateTime($value["from"]))->modify("00:00:00")->format("Y-m-d");
                    $qb->andWhere("e.created >= :dateFrom")
                        ->setParameter("dateFrom", $value["from"]);
                }

                if ($value["to"]) {
                    $value["to"] = (new \DateTime($value["to"]))->modify("23:59:59")->format("Y-m-d");
                    $qb->andWhere("e.created <= :dateTo")
                        ->setParameter("dateTo", $value["to"]);
                }
            });

        $grid->addFilterSelect("state", "State", [NULL => "---"] + Order::getStatePairs())
            ->setCondition(function ($qb, $value) {
                $qb->andWhere("e.state = :state")
                    ->setParameter("state", $value);
            });

        $grid->addFilterMultiSelect("manager", "Manager", $this->userService->getUserPairs(FALSE, FALSE, TRUE))
            ->setCondition(function ($qb, $value) {
                $qb->andWhere("e.manager = :manager")
                    ->setParameter("manager", $value[0]);
            });

        $grid->addFilterMultiSelect("supplier", "Dodavatel", $this->supplierService->getSuppliersPairs())
            ->setCondition(function ($qb, $value) {
                $qb->andWhere("e.supplier = :supplier")
                    ->setParameter("supplier", $value[0]);
            });

        $grid->addFilterMultiSelect("building", "Stavba", $this->buildingService->getBuildingsPairs())
            ->setCondition(function ($qb, $value) {
                $qb->andWhere("e.building = :building")
                    ->setParameter("building", $value[0]);
            });

        return $grid;
    }

}
