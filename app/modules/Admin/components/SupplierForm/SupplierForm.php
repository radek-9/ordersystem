<?php

namespace App\AdminModule\Components;

use App\Entities\Supplier;
use Nette\Application\UI\Form;
use h4kuna\Ares\Ares;
use h4kuna\Ares\Data;
use h4kuna\Ares\Exceptions\IdentificationNumberNotFoundException;

interface ISupplierForm {

    /** @return SupplierForm */
    function create();
}

class SupplierForm extends \App\Components\BaseControl {

    /** @var \App\Services\Supplier @autowire */
    protected $supplierService;

    /**
     * @param Supplier $supplier
     */
    public function setSupplier(Supplier $supplier) {

        $this["form"]->setDefaults([
            "id" => $supplier->id,
            "company" => $supplier->company,
            "email" => $supplier->email,
            "phone" => $supplier->phone,
            "street" => $supplier->street,
            "city" => $supplier->city,
            "zip" => $supplier->zip,
            "ino" => $supplier->ino,
            "vat" => $supplier->vat,
        ]);

        $this->template->row = $supplier;
    }

    /**
     * @return Form
     */
    protected function createComponentForm() {
        $form = new Form;

        $form->addProtection("Bezpečnosní token vypršel, odešlete formulář znovu.", 10 * 60);

        $form->addHidden("id");

        $form->addText('company', "Firma")
            ->setRequired("Pole %label je povinné");

        $form->addText('email', "Email")
            ->setRequired("Pole %label je povinné");

        $form->addText("phone", "Telefon")
            ->setRequired("Pole %label je povinné");

        $form->addText('street', "Ulice")
            ->setRequired("Pole %label je povinné");

        $form->addText('city', "Město")
            ->setRequired("Pole %label je povinné");

        $form->addText('zip', "PSČ")
            ->setRequired("Pole %label je povinné");

        $form->addText("ino", "IČO")
            ->addRule(Form::PATTERN, 'IČO musí mít 8 číslic', '([0-9]\s*){8}')
            ->setRequired("Pole %label je povinné");

        $form->addText("vat", "DIČ")
            //->setRequired("Pole %label je povinné")
        ;

        $form->addSubmit('send', 'Uložit');

        $form->onSuccess[] = [$this, "formSuccess"];

        return $form;
    }

    /**
     * @param Form $form
     */
    public function formSuccess(Form $form) {

        $values = $form->getValues();
        $this->supplierService->saveSupplier($values);
    }

    /**
     * @param $ino
     */
    public function handleGetAres($ino) {

        $ares = new Ares();
        try {
            /* @var $response  Data*/
            $response = $ares->loadData($ino);

            $this["form"]["company"]->setValue($response['company']);
            $this["form"]["street"]->setValue($response['street'] . ' ' . $response['house_number']);
            $this["form"]["city"]->setValue($response['city']);
            $this["form"]["zip"]->setValue($response['zip']);
            $this["form"]["ino"]->setValue($response['in']);
            $this["form"]["vat"]->setValue($response['tin']);

        } catch (IdentificationNumberNotFoundException $e) {
            // log identification number, why is bad? Or make nothing.
        }

        $this->redrawControl("supplierForm");
        $this->redrawControl("form");
    }

}