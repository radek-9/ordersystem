<?php

namespace App\AdminModule\Components;

use App\Entities\AclRole;
use App\Entities\WorldCountry;
use App\Facades\Mail;
use App\Services\Country;
use App\Services\User;
use Nette\Application\UI\Form;

interface IProfileForm {

    /** @return ProfileForm */
    function create();
}

class ProfileForm extends \App\Components\BaseControl {

    /** @var User @autowire */
    protected $userService;

    /** @var Country  @autowire */
    protected $countryService;

    /** @var Mail @autowire */
    protected $mailFacade;

    /**
     * @param \App\Entities\User $user
     */
    public function setUser(\App\Entities\User $user) {

        $this["form"]["email"]->setDisabled(TRUE);
        $this["form"]["aclRole"]->setDisabled(TRUE);

        $this["form"]->setDefaults([
            "id" => $user->id,
            "firstName" => $user->firstName,
            "lastName" => $user->lastName,
            "email" => $user->email,
            "phone" => $user->phone,
            "street" => $user->street,
            "city" => $user->city,
            "zip" => $user->zip,
            "country" => $user->country->id ?? NULL,
            "aclRole" => $user->aclRole->id,
            "active" => $user->active,
            "deleted" => $user->deleted,
        ]);

        $this->template->row = $user;
    }

    /**
     * @return Form
     */
    protected function createComponentForm() {
        $form = new Form;

        $form->addProtection("Bezpečnosní token vypršel, odešlete formulář znovu.", 10 * 60);

        $form->addHidden("id");

        $form->addText('firstName', "Jméno")
            ->setRequired("Pole %label je povinné");

        $form->addText('lastName', "Příjmení")
            ->setRequired("Pole %label je povinné");

        $form->addText('email', "Email")
            ->setRequired("Pole %label je povinné");

        $form->addText("phone", "Telefon")
            ->setRequired("Pole %label je povinné");

        $form->addText('street', "Ulice");

        $form->addText('city', "Město");

        $form->addText('zip', "PSČ");

        $form->addSelect('country', "Země", $this->countryService->getWorldPairs())
            ->setDefaultValue(WorldCountry::CZ);

        $form->addUpload("avatar", "Avatar")
            ->setRequired(FALSE)
            ->addRule(Form::IMAGE, "Zadejte soubor jpg, png nebo gif");

        $form->addSelect("aclRole", "Role", array_map('ucfirst', AclRole::getAdminRolePairs()))
            ->setRequired("Pole %label je povinné");

        $form->addCheckbox("active", "Aktivní účet")
            ->setDefaultValue(FALSE);

        $form->addCheckbox("deleted", "Zablokovaný účet")
            ->setDefaultValue(FALSE);

        $form->addPassword('password', "Heslo")
            ->setRequired(FALSE);

        $form->addPassword('passwordVerify', "Heslo pro kontrolu")
            ->addConditionOn($form["password"], Form::FILLED, TRUE)
            ->setRequired(TRUE)
            ->addRule(Form::EQUAL, "Hesla se neshodují", $form["password"])
            ->elseCondition()
            ->setRequired(FALSE);

        $form->addSubmit('send', 'Uložit');

        $form->onSuccess[] = [$this, "formSuccess"];

        return $form;
    }

    /**
     * @param Form $form
     * @throws \Doctrine\ORM\ORMException
     */
    public function formSuccess(Form $form) {
        $values = $form->getValues();
        $this->userService->saveUserForm($values);
    }
}