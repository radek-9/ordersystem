<?php

namespace App\AdminModule\Components;

use App\Entities\AclRole;
use App\Entities\WorldCountry;
use App\Facades\Mail;
use App\Services\Country;
use App\Services\User;
use Nette\Application\UI\Form;

interface ICustomerForm {

    /** @return CustomerForm */
    function create();
}

class CustomerForm extends \App\Components\BaseControl {

    /** @var User @autowire */
    protected $userService;

    /** @var Country  @autowire */
    protected $countryService;

    /** @var Mail @autowire */
    protected $mailFacade;

    /**
     * @param \App\Entities\User $user
     */
    public function setCustomer(\App\Entities\User $user) {

        $this["form"]["email"]->setDisabled(TRUE);

        $this["form"]->setDefaults([
            "id" => $user->id,
            "type" => $user->type,
            "billing" => $user->billing,
            "salutation" => $user->salutation,
            "firstName" => $user->firstName,
            "lastName" => $user->lastName,
            "email" => $user->email,
            "phone" => $user->phone,
            "street" => $user->street,
            "city" => $user->city,
            "zip" => $user->zip,
            "country" => $user->country->id ?? NULL,
            "aclRole" => $user->aclRole->id,
            "active" => $user->active,
            "deleted" => $user->deleted,
            "company" => $user->company,
            "ino" => $user->ino,
            "vat" => $user->vat,
        ]);

        $this->template->row = $user;
    }

    /**
     * @return Form
     */
    protected function createComponentForm() {
        $form = new Form;

        $form->addProtection("Bezpečnosní token vypršel, odešlete formulář znovu.", 10 * 60);

        $form->addHidden("id");

        $form->addSelect("type", "Typ", \App\Entities\User::getTypePairs())
            ->addCondition(Form::EQUAL, \App\Entities\User::COMPANY)
            ->toggle("company")
            ->toggle("logo")
            ->elseCondition()
            ->addCondition(Form::EQUAL, \App\Entities\User::CLIENT)
            ->toggle("avatar");

        $form->addSelect("billing", "Způsob platby", \App\Entities\User::getBillingPairs());

        $form->addSelect("salutation", "Oslovení", \App\Entities\User::getSalutationPairs())
            ->setRequired("Pole %label je povinné");

        $form->addText('firstName', "Jméno")
            ->setRequired("Pole %label je povinné");

        $form->addText('lastName', "Příjmení")
            ->setRequired("Pole %label je povinné");

        $form->addText('email', "Email")
            ->setRequired("Pole %label je povinné");

        $form->addText("phone", "Telefon")
            ->setRequired("Pole %label je povinné");

        $form->addText('street', "Ulice");

        $form->addText('city', "Město");

        $form->addText('zip', "PSČ");

        $form->addSelect('country', "Země", $this->countryService->getWorldPairs())
            ->setDefaultValue(WorldCountry::CZ);

        $form->addUpload("avatar", "Avatar")
            ->setRequired(FALSE)
            ->addRule(Form::IMAGE, "Zadejte soubor jpg, png nebo gif");

        $form->addUpload("logo", "Logo")
            ->setRequired(FALSE)
            ->addRule(Form::IMAGE, "Zadejte soubor jpg, png nebo gif");

        $form->addSelect("aclRole", "Role", array_map('ucfirst', AclRole::getCustomerRolePairs()))
            ->setRequired("Pole %label je povinné");

        $form->addCheckbox("active", "Aktivní účet")
            ->setDefaultValue(FALSE);

        $form->addCheckbox("deleted", "Zablokovaný účet")
            ->setDefaultValue(FALSE);

        $form->addPassword('password', "Heslo")
            ->setRequired(FALSE);

        $form->addPassword('passwordVerify', "Heslo pro kontrolu")
            ->addConditionOn($form["password"], Form::FILLED, TRUE)
            ->setRequired(TRUE)
            ->addRule(Form::EQUAL, "Hesla se neshodují", $form["password"])
            ->elseCondition()
            ->setRequired(FALSE);

        $form["password"]->addConditionOn($form['id'], Form::BLANK, TRUE)
            ->setRequired('Pole %label je povinné');

        // fakturační údaje

        $form->addText("company", "Firma");

        $form->addText("ino", "IČO");

        $form->addText("vat", "DIČ");

        $form->addSubmit('send', 'Uložit');

        $form->onValidate[] = [$this, "formValidate"];
        $form->onSuccess[] = [$this, "formSuccess"];

        return $form;
    }

    /**
     * @param Form $form
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function formValidate(Form $form) {
        $values = $form->getValues();

        $qb = $this->em->createQueryBuilder()
            ->select("COUNT(e.id)")
            ->from(\App\Entities\User::class, "e")
            ->andWhere("e.email = :email")
            ->setParameter("email", $values->email);

        if ($values->id) {
            $qb->andWhere("e.id != :id")
                ->setParameter("id", $values->id);
        }

        if ($qb->getQuery()->getSingleScalarResult()) {
            $form->addError("Uživatel se zadaným emailem již existuje!");
        }
    }

    /**
     * @param Form $form
     * @throws \Doctrine\ORM\ORMException
     */
    public function formSuccess(Form $form) {
        $values = $form->getValues();
        $this->userService->saveUserForm($values);
    }

    /**
     * @param $userId
     * @throws \Nette\Application\AbortException
     * @throws \Nette\Application\UI\InvalidLinkException
     */
    public function handleVerifyCustomerAccount($userId) {
        /** @var \App\Entities\User $user */
        $user = $this->em->getRepository(\App\Entities\User::class)->find($userId);

        $this->mailFacade->sendRegisterEmail($user);
        $this->presenter->flashMessage("Ověření odesláno", "success");
        $this->presenter->redirect(':Admin:User:edit', $userId);
    }

}