<?php

namespace App\AdminModule\Components;

use Nette\Application\UI\Form;

interface ILoginForm {

    /** @return LoginForm */
    function create();
}

class LoginForm extends \App\Components\BaseControl {

    /**
     * @return Form
     */
    protected function createComponentForm() {
        $form = new Form;

        $form->addProtection("Bezpečnosní token vypršel, odešlete formulář znovu.", 10 * 60);

        $form->addText('email', "Email")
            ->setType("email")
            ->setAttribute("placeholder", "Email")
            ->setRequired("Pole %label je povinné");

        $form->addPassword('password', "Heslo")
            ->setAttribute("placeholder", "Heslo")
            ->setRequired("Pole %label je povinné");

        $form->addSubmit('send', 'Přihlásit se');

        $form->onSuccess[] = [$this, "signInFormSucceeded"];

        return $form;
    }

    /**
     * @param Form $form
     * @throws \Nette\Application\AbortException
     */
    public function signInFormSucceeded(Form $form) {
        $values = $form->getValues();

        try {
            $this->user->login($values->email, $values->password);
            $this->presenter->redirect(':Admin:Default:default');

        } catch (\Nette\Security\AuthenticationException $e) {
            $this->presenter->flashMessage($e->getMessage(), "danger");
        }
    }

}
