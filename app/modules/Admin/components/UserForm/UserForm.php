<?php

namespace App\AdminModule\Components;

use App\Entities\AclRole;
use App\Entities\WorldCountry;
use App\Facades\Mail;
use App\Services\Country;
use App\Services\User;
use Nette\Application\UI\Form;

interface IUserForm {

	/** @return UserForm */
	function create();
}

class UserForm extends \App\Components\BaseControl {

	/** @var User @autowire */
	protected $userService;

    /** @var Country  @autowire */
    protected $countryService;

    /** @var Mail @autowire */
    protected $mailFacade;

    /**
     * @param \App\Entities\User $user
     */
    public function setUser(\App\Entities\User $user) {

        $this["form"]["email"]->setDisabled(TRUE);

        // superadmin
        if ($user->id === AclRole::ADMIN) {
            $this["form"]["aclRole"]->setDisabled(TRUE);
        }

        $this["form"]->setDefaults([
            "id" => $user->id,
            "firstName" => $user->firstName,
            "lastName" => $user->lastName,
            "email" => $user->email,
            "phone" => $user->phone,
            "street" => $user->street,
            "city" => $user->city,
            "zip" => $user->zip,
            "country" => $user->country->id ?? NULL,
            "aclRole" => $user->aclRole->id,
            "active" => $user->active,
            "deleted" => $user->deleted,
        ]);

        $this->template->row = $user;
    }

	/**
	 * @return Form
	 */
    protected function createComponentForm() {
        $form = new Form;

        $form->addProtection("Bezpečnosní token vypršel, odešlete formulář znovu.", 10 * 60);

        $form->addHidden("id");

        $form->addText('firstName', "Jméno")
            ->setRequired("Pole %label je povinné");

        $form->addText('lastName', "Příjmení")
            ->setRequired("Pole %label je povinné");

        $form->addText('email', "Email")
            ->setRequired("Pole %label je povinné");

        $form->addText("phone", "Telefon")
            ->setRequired("Pole %label je povinné");

        $form->addText('street', "Ulice");

        $form->addText('city', "Město");

        $form->addText('zip', "PSČ");

        $form->addSelect('country', "Země", $this->countryService->getWorldPairs())
            ->setDefaultValue(WorldCountry::CZ);

        $form->addUpload("avatar", "Avatar")
            ->setRequired(FALSE)
            ->addRule(Form::IMAGE, "Zadejte soubor jpg, png nebo gif");

        $form->addSelect("aclRole", "Role", array_map('ucfirst', AclRole::getAdminRolePairs()))
            ->setDefaultValue(AclRole::MANAGER)
            ->setRequired("Pole %label je povinné");

        $form->addCheckbox("active", "Aktivní účet")
            ->setDefaultValue(FALSE);

        $form->addCheckbox("deleted", "Zablokovaný účet")
            ->setDefaultValue(FALSE);

        $form->addPassword('password', "Heslo")
            ->setRequired(FALSE);

        $form->addPassword('passwordVerify', "Heslo pro kontrolu")
            ->addConditionOn($form["password"], Form::FILLED, TRUE)
            ->setRequired(TRUE)
            ->addRule(Form::EQUAL, "Hesla se neshodují", $form["password"])
            ->elseCondition()
            ->setRequired(FALSE);

        $form["password"]->addConditionOn($form['id'], Form::BLANK, TRUE)
            ->setRequired('Pole %label je povinné');

        $form->addSubmit('send', 'Uložit');

        $form->onSuccess[] = [$this, "formValidate"];
        $form->onSuccess[] = [$this, "formSuccess"];

        return $form;
    }

    /**
     * @param Form $form
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
	public function formValidate(Form $form) {
		$values = $form->getValues();

		$qb = $this->em->createQueryBuilder()
			->select("COUNT(e.id)")
			->from(\App\Entities\User::class, "e")
			->andWhere("e.email = :email")
			->setParameter("email", $values->email);

		if ($values->id) {
			$qb->andWhere("e.id != :id")
				->setParameter("id", $values->id);
		}

		if ($qb->getQuery()->getSingleScalarResult()) {
			$form->addError("Uživatel se zadaným emailem již existuje!");
		}
	}

    /**
     * @param Form $form
     * @throws \Doctrine\ORM\ORMException
     */
	public function formSuccess(Form $form) {
		$values = $form->getValues();
		$this->userService->saveUserForm($values);
	}

    /**
     * @param $userId
     * @throws \Nette\Application\AbortException
     * @throws \Nette\Application\UI\InvalidLinkException
     */
    public function handleVerifyUserAccount($userId) {
        /** @var \App\Entities\User $user */
        $user = $this->em->getRepository(\App\Entities\User::class)->find($userId);

        $this->mailFacade->sendRegisterEmail($user);
        $this->presenter->flashMessage("Ověření odesláno", "success");
        $this->presenter->redirect(':Admin:User:edit', $userId);
    }

}