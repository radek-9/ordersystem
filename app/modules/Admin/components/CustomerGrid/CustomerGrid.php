<?php

namespace App\AdminModule\Components;

use App\Entities\AclRole;
use App\Entities\User;
use Nette\Application\UI\Form;
use Nette\Utils\Html;

interface ICustomerGrid {

    /** @return CustomerGrid */
    function create();
}

class CustomerGrid extends \App\Components\BaseControl {

    /**
     * @return Form
     */
    protected function createComponentGrid() {
        $grid = new \App\Components\DataGrid();

        $grid->addColumnText("avatar_logo", "Avatar / Logo")
            ->setRenderer(function (User $user) {
                if ($user->isTypeClient()) {
                    return Html::el("img", [
                        "class" => "img-responsive",
                        "src" => "/" . $user->getAvatarPath(),
                        "style" => "max-height: 30px",
                    ]);
                } elseif ($user->isTypeCompany()) {
                    return Html::el("img", [
                        "class" => "img-responsive",
                        "src" => "/" . $user->getLogoPath(),
                        "style" => "max-height: 30px",
                    ]);
                }
            });

        $grid->addColumnText("firstName", "Jméno")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("lastName", "Příjmení")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("email", "Email")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("phone", "Telefon")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("company", "Firma")
            ->setSortable()
            ->setSortableCallback([$this, "baseSortCallback"]);

        $grid->addColumnText("type", "Typ")
            ->setRenderer(function(User $user) {
                return User::getTypeName($user->billing);
            });

        $grid->addColumnText("billing", "Způsob platby")
            ->setRenderer(function(User $user) {
                return User::getBillingName($user->billing);
            });

        $grid->addColumnNumber("active", "Aktivovaný účet")
            ->setRenderer(function (User $user) {
                return $user->active ? "Ano" : "Ne";
            });

        $grid->addColumnNumber("deleted", "Odstraněný účet")
            ->setRenderer(function (User $user) {
                return $user->deleted ? "Ano" : "Ne";
            });

        if ($this->user->allowed("customer.edit")) {
            $grid->addAction("edit", "edit")
                ->setRenderer(function (User $user) {
                    return Html::el("a", [
                        "href" => $this->presenter->link("editCustomer", $user->id),
                        "class" => "btn btn-success btn-xs",
                    ])->setHtml('<i class="fa fa-pencil"></i>');
                });
        }

        $grid->setDataSource(
            $this->em->createQueryBuilder()
                ->select("e")
                ->from(User::class, "e")
                ->andWhere("e.aclRole = :aclRole")
                ->setParameter("aclRole", AclRole::CUSTOMER)
                ->orderBy("e.id", "DESC")
        );

        // filtr
        // -------------------------------------------------------------------------------------------------------------

        foreach (["firstName", "lastName", "email", "phone", "company"] as $column) {
            $grid->addFilterText($column, $column)
                ->setCondition(function ($qb, $value) use ($column) {
                    $qb->andWhere("e.$column LIKE :$column")
                        ->setParameter($column, "%" . $value . "%");
                });
        };

        foreach (["active", "deleted"] as $column) {
            $grid->addFilterSelect($column, $column, [
                NULL => "---",
                1 => "Ano",
                0 => "Ne",
            ])
                ->setCondition(function ($qb, $value) use ($column) {
                    $qb->andWhere("e.$column = :$column")
                        ->setParameter($column, $value);
                });
        };

        $grid->addFilterSelect("type", "type", array_merge([NULL => "---"], User::getTypePairs()))
            ->setCondition(function ($qb, $value) {

                if ($value == User::CLIENT) {
                    $qb->andWhere("e.type = :type")
                        ->setParameter("type", User::CLIENT);

                } else if ($value == User::COMPANY) {
                    $qb->andWhere("e.type = :type")
                        ->andWhere("e.billing != :typeBilling")
                        ->setParameter("type", User::COMPANY)
                        ->setParameter("typeBilling", User::BILLING_INVOICE);
                } else {
                    $qb->andWhere("e.type = :type")
                        ->andWhere("e.billing = :typeBilling")
                        ->setParameter("type", User::COMPANY)
                        ->setParameter("typeBilling", User::BILLING_INVOICE);
                }
            });

        $grid->addFilterSelect("billing", "billing", array_merge([NULL => "---"], User::getBillingPairs()))
            ->setCondition(function ($qb, $value) {
                $qb->andWhere("e.billing = :billing")
                    ->setParameter("billing", $value);
            });

        return $grid;
    }

}
