<?php

namespace App\FrontModule\Components;

use Nette\Application\UI\Form;

interface ILoginForm {

	/** @return LoginForm */
	function create();
}

class LoginForm extends \App\Components\BaseControl {

	/**
	 * @return Form
	 */
	protected function createComponentForm() {
		$form = new Form;

		$form->addProtection("If the security token has expired, submit the form again", 10 * 60);

		$form->addEmail('email', "email")
			->setHtmlAttribute("placeholder", "email")
			->setRequired("required");

		$form->addPassword('password', "password")
			->setHtmlAttribute("placeholder", "password")
			->setRequired("required");

		$form->addSubmit('send', 'send');

		$form->onSuccess[] = [$this, "formSuccess"];

		return $form;
	}

    /**
     * @param Form $form
     * @throws \Nette\Application\AbortException
     */
	public function formSuccess(Form $form) {
		$values = $form->getValues();

		try {
			$this->user->login($values->email, $values->password);

		} catch (\Nette\Security\AuthenticationException $e) {
			$this->presenter->flashMessage($e->getMessage(), "danger");
			$this->presenter->redirect("this");
		}
	}

}
