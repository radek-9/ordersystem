<?php

namespace App\FrontModule\Components;

use App\Entities\User;
use App\Facades\Mail;
use Nette\Application\UI\Form;

interface ILostPasswordForm {

	/** @return LostPasswordForm */
	function create();
}

class LostPasswordForm extends \App\Components\BaseControl {

	/** @var Mail @autowire */
	protected $mailFacade;

	/**
	 * @return Form
	 */
	protected function createComponentForm() {
		$form = new Form;

		$form->addProtection("If the security token has expired, submit the form again", 10 * 60);

		$form->addEmail('email', "email")
			->setHtmlAttribute("placeholder", "email")
			->setRequired("required");

		$form->addReCaptcha('captcha', 'recaptcha', "recaptchaError");

		$form->addSubmit('send', 'send');

		$form->onSuccess[] = [$this, "formSuccess"];

		return $form;
	}

    /**
     * @param Form $form
     * @throws \Nette\Application\AbortException
     * @throws \Nette\Application\UI\InvalidLinkException
     */
	public function formSuccess(Form $form) {

		$values = $form->getValues();

		/** @var User $user */
		$user = $this->em->getRepository(User::class)->findOneBy([
			"email" => $values->email,
		]);


		if (!$user) {
			$this->presenter->flashMessage("email error", "danger");
			$this->presenter->redirect("this");
		}

		$this->mailFacade->sendLostPasswordEmail($user);

		$this->presenter->flashMessage("email success", "success");
		$this->presenter->redirect("this");
	}

}
