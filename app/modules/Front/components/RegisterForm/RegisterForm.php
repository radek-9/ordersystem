<?php

namespace App\FrontModule\Components;

use App\Entities\WorldCountry;
use App\Facades\Mail;
use App\Model\Phone;
use App\Services\Country;
use App\Services\User;
use Nette\Application\UI\Form;

interface IRegisterForm {

	/** @return RegisterForm */
	function create();
}

class RegisterForm extends \App\Components\BaseControl {

	/** @var Country @autowire */
	protected $countryService;

	/** @var User @autowire */
	protected $userService;

	/** @var Mail @autowire */
	protected $mailFacade;

	/**
	 * @return Form
	 */
	protected function createComponentForm() {
		$form = new Form;

		$form->addProtection("If the security token has expired, submit the form again", 10 * 60);

		$form->addRadioList("type", "type", \App\Entities\User::getTypePairs())
			->setDefaultValue(\App\Entities\User::CLIENT)
			->getLabelPrototype()->class = 'col-md-6';

		$form["type"]->addCondition(Form::EQUAL, \App\Entities\User::CLIENT)
			->toggle("register-client");

		$form["type"]->addCondition(Form::EQUAL, \App\Entities\User::COMPANY)
			->toggle("register-company");

		// Klient
		// -------------------------------------------------------------------------------------------------------------

		$client = $form->addContainer("client");

		$client->addText("firstName", "first name")
			->setHtmlAttribute("placeholder", "first name")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::CLIENT)
			->setRequired("first name required");

		$client->addText("lastName", "last name")
			->setHtmlAttribute("placeholder", "last name")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::CLIENT)
			->setRequired("last name required");

		$client->addSelect("phonePrefix", "phone", Phone::getPrefix())
			->setDefaultValue("420")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::CLIENT)
			->setRequired("phone prefix required");

		$client->addText("phone", "phone")
			->setHtmlAttribute("placeholder", "phone")
			->setType("tel")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::CLIENT)
			->setRequired("phone required");

        $client->addText('email', 'email')
            ->setHtmlAttribute("placeholder", "email")
            ->setRequired(FALSE)
            ->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::CLIENT)
            ->addRule(Form::EMAIL, 'enter a valid email address')
            ->setRequired("email required");

		$client->addText("street", "street")
			->setHtmlAttribute("placeholder", "street")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::CLIENT)
			->setRequired("street required");

		$client->addText("city", "city")
			->setHtmlAttribute("placeholder", "city")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::CLIENT)
			->setRequired("city required");

		$client->addText("zip", "zip")
			->setHtmlAttribute("placeholder", "zip")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::CLIENT)
			->setRequired("zip required");

        $client->addSelect("country", "country", $this->countryService->getWorldPairs())
			->setHtmlAttribute("placeholder", "country")
			->setDefaultValue(WorldCountry::CZ)
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::CLIENT)
			->setRequired("country required");

		// Firma
		// -------------------------------------------------------------------------------------------------------------

		$company = $form->addContainer("company");

		$company->addText("firstName", "first name")
			->setHtmlAttribute("placeholder", "first name")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::COMPANY)
			->setRequired("first name required");

		$company->addText("lastName", "last name")
			->setHtmlAttribute("placeholder", "last name")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::COMPANY)
			->setRequired("last name required");

		$company->addSelect("phonePrefix", "phone", Phone::getPrefix())
			->setDefaultValue("420")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::COMPANY)
			->setRequired("phone prefix required");

		$company->addText("phone", "phone")
			->setHtmlAttribute("placeholder", "phone")
			->setType("tel")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::COMPANY)
			->setRequired("phone required");

        $company->addText('email', 'email')
            ->setHtmlAttribute("placeholder", "email")
            ->setRequired(FALSE)
            ->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::COMPANY)
            ->addRule(Form::EMAIL, 'enter a valid email address')
            ->setRequired("email required");

		$company->addText('company', "company")
			->setHtmlAttribute("placeholder", "company")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::COMPANY)
			->setRequired("company required");

		$company->addText('ino', "ino")
			->setHtmlAttribute("placeholder", "ino")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::COMPANY)
			->setRequired("ino required");

		$company->addText('vat', "vat")
			->setHtmlAttribute("placeholder", "vat");

		$company->addText("street", "street")
			->setHtmlAttribute("placeholder", "street")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::COMPANY)
			->setRequired("street required");

		$company->addText("city", "city")
			->setHtmlAttribute("placeholder", "city")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::COMPANY)
			->setRequired("city required");

		$company->addText("zip", "zip")
			->setHtmlAttribute("placeholder", "zip")
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::COMPANY)
			->setRequired("zip required");

		$company->addSelect("country", "country", $this->countryService->getWorldPairs())
			->setHtmlAttribute("placeholder", "country")
			->setDefaultValue(WorldCountry::CZ)
			->setRequired(FALSE)
			->addConditionOn($form["type"], Form::EQUAL, \App\Entities\User::COMPANY)
			->setRequired("country required");

		// Heslo
		// -------------------------------------------------------------------------------------------------------------

		$form->addPassword('password', "password")
			->setHtmlAttribute("placeholder", "password")
			->setRequired("password required");

		$form->addPassword('passwordVerify', "password verify")
			->addRule(Form::EQUAL, "password verify error", $form["password"])
			->setHtmlAttribute("placeholder", "password verify")
			->setRequired("password verify required");

		$form->addCheckbox("consent")
			->setRequired("consent required");

		$form->addSubmit('send', 'send');

        $form->onValidate[] = [$this, "formValidate"];
        $form->onSuccess[] = [$this, "formSuccess"];

		return $form;
	}

	/**
	 * @param Form $form
	 */
	public function formValidate(Form $form) {
		$values = $form->getValues();

		if ($values->type === \App\Entities\User::CLIENT) {
			$email = $values->client->email;

		} else {
			$email = $values->company->email;
		}

		$row = $this->em->getRepository(\App\Entities\User::class)->findBy([
			"email" => $email,
		]);

		if ($row) {
			$form->addError("email exist");
		}
	}

    /**
     * @param Form $form
     * @throws \Doctrine\ORM\ORMException
     * @throws \Nette\Application\UI\InvalidLinkException
     */
	public function formSuccess(Form $form) {
		$values = $form->getValues();

		/** @var \App\Entities\User $user */
		$user = $this->userService->register($values);

		// odeslání emailu
		$this->mailFacade->sendRegisterEmail($user);
	}

}
