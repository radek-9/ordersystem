<?php

namespace App\FrontModule\Components;

use App\Model\Phone;
use App\Services\Country;
use App\Services\User;
use Nette\Application\UI\Form;

interface IProfileForm {

	/** @return ProfileForm */
	function create();
}

class ProfileForm extends \App\Components\BaseControl {

	/** @var User @autowire */
	protected $userService;

	/** @var Country @autowire */
	protected $countryService;

	/**
	 * @return Form
	 */
	protected function createComponentForm() {
		$form = new Form;

		$form->addProtection("If the security token has expired, submit the form again", 10 * 60);

		$form->addSelect("salutation", "salutation", \App\Entities\User::getSalutationPairs())
			->setRequired("required");

		$form->addText("firstName", "first name")
			->setHtmlAttribute("placeholder", "first name")
			->setRequired("required");

		$form->addText("lastName", "last name")
			->setHtmlAttribute("placeholder", "last name")
			->setRequired("required");

		$form->addSelect("phonePrefix", "phone", Phone::getPrefix())
			->setDefaultValue("420")
			->setRequired("required");

		$form->addText("phone", "phone")
			->setHtmlAttribute("placeholder", "phone")
			->setType("tel")
			->setRequired("required");

		$form->addEmail('email', "email")
			->setHtmlAttribute("placeholder", "email")
			->setDisabled(TRUE)
			->setRequired("required");

		$form->addText("street", "street")
			->setHtmlAttribute("placeholder", "street")
			->setRequired("required");

		$form->addText("city", "city")
			->setHtmlAttribute("placeholder", "city")
			->setRequired("required");

		$form->addText("zip", "zip")
			->setHtmlAttribute("placeholder", "zip")
			->setRequired("required");

		$form->addSelect("country", "country", $this->countryService->getWorldPairs())
			->setRequired("required");

        if ($this->user->identity->isTypeClient()) {

            $form->addUpload("avatar")
                ->setRequired(FALSE)
                ->addRule(Form::IMAGE, "avatar image error");
        }

		if ($this->user->identity->isTypeCompany()) {

			$form->addText("company", "company")
				->setHtmlAttribute("placeholder", "company")
				->setRequired("required");

			$form->addText("ino", "ino")
				->setHtmlAttribute("placeholder", "ino")
				->setRequired("required");

			$form->addText("vat", "vat")
				->setHtmlAttribute("placeholder", "vat");

            $form->addUpload("logo")
                ->setRequired(FALSE)
                ->addRule(Form::IMAGE, "logo image error");
		}

		$form->addPassword('password', "password")
			->setHtmlAttribute("placeholder", "password")
			->setRequired(FALSE);

		$form->addPassword('passwordVerify', "password verify")
			->setHtmlAttribute("placeholder", "password verify")
			->addConditionOn($form["password"], Form::FILLED, TRUE)
				->setRequired(TRUE)
				->addRule(Form::EQUAL, "password verify error", $form["password"])
			->elseCondition()
			->setRequired(FALSE);

		$form->addSubmit('send', 'send');

		$form->onSuccess[] = [$this, "formSuccess"];

		return $form;
	}

    /**
     * @param Form $form
     * @throws \Doctrine\ORM\ORMException
     */
	public function formSuccess(Form $form) {

		$values = $form->values;
		$this->userService->updateProfile($values);
	}

	/**
	 * @param \App\Entities\User $user
	 */
	public function mapValues(\App\Entities\User $user) {

		$this["form"]->setDefaults([
			"firstName" => $user->firstName,
			"lastName" => $user->lastName,
			"phonePrefix" => Phone::getPrefixFromPhone($user->phone),
			"phone" => Phone::removePrefixFromPhone($user->phone),
			"email" => $user->email,
			"salutation" => $user->salutation,

			"street" => $user->street,
			"city" => $user->city,
			"zip" => $user->zip,
			"country" => $user->country->id ?? NULL,

			"company" => $user->company,
			"ino" => $user->ino,
			"vat" => $user->vat,
		]);
	}

}
