<?php

namespace App\FrontModule\Components;

use App\Entities\User;
use Nette\Application\UI\Form;

interface INewPasswordForm {

	/** @return NewPasswordForm */
	function create();
}

class NewPasswordForm extends \App\Components\BaseControl {

	/**
	 * @return Form
	 */
	protected function createComponentForm() {
		$form = new Form;

		$form->addProtection("If the security token has expired, submit the form again", 10 * 60);

		$form->addHidden("id");

		$form->addPassword('password', "password")
			->setHtmlAttribute("placeholder", "password")
			->setRequired("required");

		$form->addPassword('passwordVerify', "password verify")
			->addRule(Form::EQUAL, "password verify error", $form["password"])
			->setHtmlAttribute("placeholder", "password verify")
			->setRequired("required");

		$form->addSubmit('send', 'send');

		$form->onSuccess[] = [$this, "formSuccess"];

		return $form;
	}

    /**
     * @param Form $form
     * @throws \Nette\Application\AbortException
     * @throws \Nette\Application\BadRequestException
     */
	public function formSuccess(Form $form) {
		$values = $form->getValues();

		/** @var User $user */
		$user = $this->em->getRepository(User::class)->find($values->id);

		if (!$user) {
			$this->error();
		}

		$user->setNewPassword($values->password);
		$user->active = TRUE;

		$this->em->flush($user);

		$this->presenter->flashMessage("newPassword success", "success");
		$this->presenter->redirect("in");
	}

}
