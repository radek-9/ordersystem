<?php

namespace App\FrontModule\Presenters;

use Nette\Http\Request;
use Nette\Http\Response;

class BasePresenter extends \App\Presenters\BasePresenter {

	/** @var Request @inject */
	public $httpRequest;

	/** @var Response @inject */
	public $httpResponse;

	public function startup() {

	    parent::startup();
	}

	protected function beforeRender() {

	    parent::beforeRender();

		$firstLoading = $this->httpRequest->getCookie("firstLoading");

		if ($firstLoading) {
			$this->httpResponse->setCookie('firstLoading', 0, '365 days');
			$this->template->firstLoading = TRUE;
		}
	}

    public function actionDefault() {

    }
}