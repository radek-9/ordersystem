<?php

namespace App\FrontModule\Presenters;

use App\Entities\User;
use App\FrontModule\Components\ILoginForm;
use App\FrontModule\Components\ILostPasswordForm;
use App\FrontModule\Components\INewPasswordForm;
use App\FrontModule\Components\IRegisterForm;

class SignPresenter extends BasePresenter {

	/** @var \App\Services\User @autowire */
	protected $userService;

    /**
     * @throws \Nette\Application\AbortException
     */
	public function actionIn() {
		if ($this->user->isLoggedIn()) {
			$this->redirect("Default:");
		}
	}

    /**
     * @throws \Nette\Application\AbortException
     */
	public function actionRegister() {
		if ($this->user->isLoggedIn()) {
			$this->redirect("Default:");
		}
	}

    /**
     * @throws \Nette\Application\AbortException
     */
	public function actionOut() {
		$this->user->logout(TRUE);
        $this->flashMessage("logout success", "success");
		$this->redirect("Sign:in");
	}

	/**
	 * @param IRegisterForm $factory
	 * @return \App\FrontModule\Components\RegisterForm
	 */
	protected function createComponentRegisterForm(IRegisterForm $factory) {
		$control = $factory->create();
		$control["form"]->onSuccess[] = function () {
			$this->flashMessage("register success", "success");
			$this->redirect("in");
		};

		return $control;
	}

	/**
	 * @param ILoginForm $factory
	 * @return \App\FrontModule\Components\LoginForm
	 */
	protected function createComponentLoginForm(ILoginForm $factory) {
		$control = $factory->create();
		$control["form"]->onSuccess[] = function () {
            $this->flashMessage("login success", "success");
			$this->redirect("Default:");
		};

		return $control;
	}

	/**
	 * @param ILostPasswordForm $factory
	 * @return \App\FrontModule\Components\LostPasswordForm
	 */
	protected function createComponentLostPasswordForm(ILostPasswordForm $factory) {
		return $factory->create();
	}

	/**
	 * @param INewPasswordForm $factory
	 * @return \App\FrontModule\Components\NewPasswordForm
	 */
	protected function createComponentNewPasswordForm(INewPasswordForm $factory) {
		return $factory->create();
	}

    /**
     * Aktivace účtu
     *
     * @param $email
     * @param $hash
     * @throws \Nette\Application\AbortException
     * @throws \Nette\Application\BadRequestException
     */
    public function actionActivateAccount($email, $hash) {

        if (!$email ||!$hash) {
            $this->error();
        }

        /** @var User $user */
        $user = $this->em->getRepository(User::class)->findOneBy([
            "email" => $email,
        ]);

        if (!$user || $user->active || md5($user->id) != $hash) {
            $this->error();
        }

        $user->active = TRUE;
        $this->em->flush($user);

        $this->flashMessage("activate account success", "success");
        $this->redirect("in");
    }

    /**
     * @param $email
     * @param $hash
     * @throws \Nette\Application\BadRequestException
     */
	public function actionNewPassword($email, $hash) {

		if (!$email || !$hash) {
			$this->error();
		}

		/** @var User $user */
		$user = $this->em->getRepository(User::class)->findOneBy([
			"email" => $email,
		]);

		if (!$user || md5($user->id) != $hash) {
			$this->error();
		}

		$this["newPasswordForm"]["form"]["id"]->setValue($user->id);
	}
}