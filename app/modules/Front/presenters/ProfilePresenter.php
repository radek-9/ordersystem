<?php

namespace App\FrontModule\Presenters;

use App\FrontModule\Components\IProfileForm;
use App\Services\User;

class ProfilePresenter extends BasePresenter {

    /** @var User @autowire */
    protected $userService;

    public function actionDefault() {

    }

    public function actionEdit() {
        $this["profileForm"]->mapValues($this->user->identity);
    }

    /**
     * @param IProfileForm $factory
     * @return \App\FrontModule\Components\ProfileForm
     */
    protected function createComponentProfileForm(IProfileForm $factory) {
        $control = $factory->create();
        $control["form"]->onSuccess[] = function () {
            $this->flashMessage("profile edit success", "success");
            $this->redirect("Profile:");
        };

        return $control;
    }

    /**
     * @throws \Nette\Application\AbortException
     */
    public function handleDeleteAccount() {

        /** @var \App\Entities\User $user */
        $user = $this->user->identity;

        $user->deleted = TRUE;
        $this->em->flush($user);

        $this->flashMessage("close account success");
        $this->redirect("Sign:out");
    }
}