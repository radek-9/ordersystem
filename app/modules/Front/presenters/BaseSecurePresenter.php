<?php

namespace App\FrontModule\Presenters;

class BaseSecurePresenter extends BasePresenter {

	public function startup() {

		parent::startup();

		if (!$this->user->isLoggedIn()) {
			$this->flashMessage("not logged in", "info");
			$this->redirect("Sign:in");
		}
	}
}