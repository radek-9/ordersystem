<?php

namespace App\Model;

use Nette;

class LinkGenerator {

    /** @var \Nette\Application\LinkGenerator */
    private $linkGenerator;

    /**
     * @param Nette\Application\LinkGenerator $linkGenerator
     */
    public function __construct(\Nette\Application\LinkGenerator $linkGenerator) {
        $this->linkGenerator = $linkGenerator;
    }

    /**
     * @param $dest
     * @param array $params
     * @return string
     * @throws Nette\Application\UI\InvalidLinkException
     */
    public function link($dest, array $params = array()) {
        // Remove leading colong, currently broken in LinkGenerator
        $dest = preg_replace("/^:/", "", $dest);
        return $this->linkGenerator->link($dest, $params);
    }

}
