<?php

namespace App\Facades;

use App\Entities\Order;
use App\Entities\OrderDocument;
use App\Entities\User;
use Nette\Mail\IMailer;
use Nette\Mail\Message;
use Nette\Mail\SendException;
use Nette\Mail\SendmailMailer;
use Nette\Mail\SmtpException;
use Nette\Utils\ArrayHash;
use Tracy\ILogger;

class Mail extends Base
{

	/** @var \App\Model\LinkGenerator @inject */
	public $linkGenerator;

	/** @var \Nette\Bridges\ApplicationLatte\ILatteFactory @inject */
	public $latteFactory;

	/** @var IMailer @inject */
	public $mailer;

	/** @var string */
	protected $sender;

	/** @var array */
	protected $adminEmails = [];

    /**
	 * @param $emails
	 */
	public function setAdminEmails($emails){
		foreach (explode(",", $emails) as $email) {
			$this->adminEmails[] = $email;
		}
	}

	/**
	 * @param string $sender
	 */
	public function setSender($sender) {
		$this->sender = $sender;
	}

	/**
	 * @param $message
	 * @return \Latte\Runtime\IHtmlString|\Nette\Utils\IHtmlString|string
	 */
	protected function _($message) {
		return $message;
	}

	/**
	 * @param $template
	 * @param array $params
	 * @return Message
	 */
	protected function createMessage($template, array $params = NULL) {

		$latte = $this->latteFactory->create();

		// nainstalujme do $latte makra {link} a n:href
		\Nette\Bridges\ApplicationLatte\UIMacros::install($latte->getCompiler());

		// pro Latte 2.4
		$latte->addProvider('uiControl', $this->linkGenerator);

		$template = $latte->renderToString(EMAIL_PATH . $template . ".latte", $params);

		// převod class do inline stylů
		$html = (new \TijsVerkoyen\CssToInlineStyles\CssToInlineStyles())->convert(
			$template, file_get_contents(CSS_PATH . '/email.css')
		);

		$message = new Message;
		$message->setFrom('objednavky.mtbsport@seznam.cz', 'test');
		$message->setHtmlBody($html);

		return $message;
	}

	/**
	 * @param Message $message
	 */
	protected function send(Message $message) {
		try {
			$this->mailer->send($message);

		} catch (\Exception $e) {
			if (!$e instanceof SmtpException && !$e instanceof SendmailMailer && !$e instanceof SendException) {
				\Tracy\Debugger::log($e, ILogger::EXCEPTION);
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

    /**
     * Odešle registrační email s odkazem pro aktivaci účtu
     *
     * @param User $user
     * @param null $password
     * @throws \Nette\Application\UI\InvalidLinkException
     */
	public function sendRegisterEmail(User $user, $password = NULL) {

		$message = $this->createMessage("register", [
			"user" => $user,
			"link" => $this->linkGenerator->link("Front:Sign:activateAccount", ["email" => $user->email, "hash" => md5($user->id)]),
			"salutation" => $this->getSalutation($user),
			"password" => $password,
		]);

		$message->setSubject($this->_("app.email.register.subject"));
		$message->setFrom("objednavky.mtbsport@seznam.cz");
		$message->addTo($user->email);

		$this->send($message);
	}

    /**
     * Odešle email s informacemi pro obnovení hesla
     *
     * @param User $user
     * @throws \Nette\Application\UI\InvalidLinkException
     */
	public function sendLostPasswordEmail(User $user) {

		$message = $this->createMessage("lostPassword", [
			"salutation" => $this->getSalutation($user),
			"link" => $this->linkGenerator->link("Front:Sign:newPassword", ["email" => $user->email, "hash" => md5($user->id)])
		]);

		$message->setSubject($this->_("app.email.lostPassword.subject"));
		$message->addTo($user->email);

		$this->send($message);
	}

	/**
	 * Potvrzení odeslání kontaktního formuláře
	 *
	 * @param ArrayHash $values
	 */
	public function sendContactClientEmail(ArrayHash $values) {

		$message = $this->createMessage("contactClient", [
			"values" => $values,
		]);

		$message->setSubject($this->_("app.email.contactClient.subject"));
		$message->addTo($values->email);

		$this->send($message);
	}

	/**
	 * Přeposlání kontaktního formuláře adminovi
	 *
	 * @param ArrayHash $values
	 * @param string $url
	 */
	public function sendContactAdminEmail(ArrayHash $values, $url) {

		$message = $this->createMessage("contactAdmin", [
			"values" => $values,
		]);

		$message->setSubject($this->_("app.email.contactAdmin.subject") . " " . $url);

        foreach ($this->adminEmails as $email) {
            $message->addTo($email);
        }

		$this->send($message);
	}

    /**
     * Odeslání objednávky
     *
     * @param Order $order
     * @param $pdfName
     * @param $content
     */
    public function sendOrderEmail(Order $order, $pdfName, $content) {

        $message = $this->createMessage("order", [
            "order" => $order,
        ]);

        $message->setSubject("Objednávka");

        $message->addAttachment($pdfName, $content);

        /** @var OrderDocument $document */
        foreach ($order->documents as $document) {
            $message->addAttachment($document->path);
        }

        $message->addTo($order->supplier->email);

        $this->send($message);
    }

}