<?php

namespace App\Facades;

use Nettrine\ORM\EntityManagerDecorator as EntityManager;

class Base
{
	/** @var EntityManager @inject */
	public $em;

}