<?php

namespace App\Services;

use App\Entities\AclRole;
use App\Entities\WorldCountry;
use Nette\Utils\ArrayHash;
use Nette\Utils\Image;
use Nette\Utils\ImageException;

class User extends Base {

	/** @var \App\Security\User @inject */
	public $user;

    /**
     * @param bool $onlyClient
     * @param bool $onlyCompany
     * @return mixed
     */
	public function getUsers($onlyClient = FALSE, $onlyCompany = FALSE) {

		$qb = $this->em->createQueryBuilder()
			->select("e")
			->from(\App\Entities\User::class, "e")
            ->leftJoin(AclRole::class, "a")
            ->andWhere("e.deleted = 0")
            ->andWhere("a.id != :admin")
            ->setParameter("admin", AclRole::ADMIN);

		if ($onlyClient) {
            $qb->andWhere("e.type = :client")
                ->setParameter("client", \App\Entities\User::CLIENT);
        }

        if ($onlyCompany) {
            $qb->andWhere("e.type = :company")
                ->setParameter("company", \App\Entities\User::COMPANY);
        }

		return $qb->getQuery()->getResult();
	}

	/**
	 * @return array
	 */
	public function getRolePairs() {
		$roles = [];

		$qb = $this->em->createQueryBuilder()
			->select("e")
			->from(AclRole::class, "e")
			->andWhere("e.id != :admin")
			->setParameter("admin", AclRole::ADMIN)
			->getQuery();

		/** @var AclRole $aclRole */
		foreach ($qb->getResult() as $aclRole) {
			$roles[$aclRole->id] = $aclRole->code;
		}

		return $roles;
	}

    /**
     * Registrace uživatele
     *
     * @param ArrayHash $values
     * @return \App\Entities\User
     * @throws \Doctrine\ORM\ORMException
     */
	public function register(ArrayHash $values) {

        $user = new \App\Entities\User;

        // klient
        if ($values->type === \App\Entities\User::CLIENT) {
            self::fillBasicParams($user, $values->client);
            $user->country = $this->em->getReference(WorldCountry::class, $values->client->country);
            $user->type = \App\Entities\User::CLIENT;

        // firma
        } else {
            self::fillBasicParams($user, $values->company);
            $user->country = $this->em->getReference(WorldCountry::class, $values->company->country);
            $user->type = \App\Entities\User::COMPANY;
        }

        $user->setNewPassword($values->password);
        $user->aclRole = $this->em->getReference(AclRole::class, AclRole::CUSTOMER);

        $this->em->persist($user);
        $this->em->flush($user);

        return $user;
	}

    /**
     * @param ArrayHash $values
     * @param $photo
     * @return \App\Entities\User
     * @throws \Doctrine\ORM\ORMException
     */
	public function registerFacebookUser(ArrayHash $values, $photo) {

		$user = new \App\Entities\User;
		$user->aclRole = $this->em->getReference(AclRole::class, AclRole::CUSTOMER);
		$user->firstName = $values->first_name;
		$user->lastName = $values->last_name;
		$user->email = $values->email;
		$user->active = TRUE;
		$user->type = \App\Entities\User::CLIENT;

		// uložení fotky
		if ($photo) {

			try {
				$image = Image::fromString($photo);
				$user->avatar = self::storePhoto($image);

			} catch (ImageException $e) {
				\Tracy\Debugger::log($e);
			}
		}

		$this->em->persist($user);
		$this->em->flush();

		return $user;
	}

    /**
     * Editace profilu
     *
     * @param ArrayHash $values
     * @throws \Doctrine\ORM\ORMException
     */
	public function updateProfile(ArrayHash $values) {

		/** @var \App\Entities\User $user */
		$user = $this->em->getRepository(\App\Entities\User::class)->find($this->user->identity->getId());

		self::fillBasicParams($user, $values);

		$user->country = $this->em->getReference(WorldCountry::class, $values->country);

		if ($values->password && $values->password === $values->passwordVerify) {
			$user->setNewPassword($values->password);
		}

		if (isset($values->avatar) && $values->avatar->isOk() && $values->avatar->isImage()) {
			$user->avatar = self::storeFile($values->avatar);
		}

        if (isset($values->logo) && $values->logo->isOk() && $values->logo->isImage()) {
            $user->logo = self::storeFile($values->logo);
        }

		$this->em->flush($user);

	}

	/**
	 * @param \App\Entities\User $user
	 * @param ArrayHash $values
	 */
	protected static function fillBasicParams(\App\Entities\User $user, ArrayHash $values) {
		foreach ([
			"salutation",
			"type",

			"firstName",
			"lastName",
			"email",
			"phone",
			"street",
			"city",
			"zip",

			"company",
			"ino",
			"vat",

		 	"active",
		 	"deleted",
		] as $column) {
			if (array_key_exists($column, $values)) {
				$user->$column = $values->$column;
			}
		}

		if (!empty($values->phonePrefix) && !empty($values->phone)) {
			$user->phone = "+" . $values->phonePrefix . $values->phone;
		}
	}

    /**
     * @param ArrayHash $values
     * @throws \Doctrine\ORM\ORMException
     */
	public function saveUserForm(ArrayHash $values) {

		if ($values->id) {
			/** @var \App\Entities\User $user */
			$user = $this->em->getRepository(\App\Entities\User::class)->find($values->id);

		} else {
			$user = new \App\Entities\User;
			$this->em->persist($user);
		}

		self::fillBasicParams($user, $values);

		if (!empty($values->avatar) && $values->avatar->isOk() && $values->avatar->isImage()) {
			$user->avatar = self::storeFile($values->avatar);
		}

        if (!empty($values->logo) && $values->logo->isOk() && $values->logo->isImage()) {
            $user->logo = self::storeFile($values->logo);
        }

		if (isset($values->note)) {
			$user->note = $values->note;
		}

		if (isset($values->aclRole)) {
		    $user->aclRole = $this->em->getReference(AclRole::class, $values->aclRole);
        }
        $user->country = $this->em->getReference(WorldCountry::class, $values->country);

        // způsob platby firmy
        if (isset($values->type) && $values->type == \App\Entities\User::COMPANY && $values->billing) {
            $user->billing = $values->billing;

        } else {
            $user->billing = \App\Entities\User::BILLING_ONLINE;
        }

		// změna hesla
		if ($values->password) {
			$user->setNewPassword($values->password);
		}

		$this->em->flush($user);
	}

    /**
     * @param bool $onlyClient
     * @param bool $onlyCompany
     * @param bool $onlyManager
     * @return array
     */
    public function getUserPairs($onlyClient = FALSE, $onlyCompany = FALSE, $onlyManager = FALSE) {

        $qb = $this->em->createQueryBuilder()
            ->select("e")
            ->from(\App\Entities\User::class, "e");

        if ($onlyClient) {
            $qb->andWhere("e.aclRole = :aclRole")
                ->setParameter("aclRole", AclRole::CUSTOMER)
                ->andWhere("e.type = :client")
                ->setParameter("client", \App\Entities\User::CLIENT);
        }

        if ($onlyCompany) {
            $qb->andWhere("e.aclRole = :aclRole")
                ->setParameter("aclRole", AclRole::CUSTOMER)
                ->andWhere("e.type = :company")
                ->setParameter("company", \App\Entities\User::COMPANY);
        }

        if ($onlyManager) {
            $qb->andWhere("e.aclRole = :aclRole")
                ->setParameter("aclRole", AclRole::MANAGER);
        }

        $results = $qb->getQuery()
            ->getResult();

        $data = [];

        /** @var \App\Entities\User $user */
        foreach ($results as $user) {
            $data[$user->id] = $user->getFullName();
        }

        return $data;
    }

    /**
     * @param bool $onlyClient
     * @param bool $onlyCompany
     * @return array
     */
	/*public function getUserPairs($onlyClient = FALSE, $onlyCompany = FALSE) {
		$results = $this->em->createQueryBuilder()
			->select("e")
			->from(\App\Entities\User::class, "e")
			->andWhere("e.aclRole = :aclRole")
			->setParameter("aclRole", AclRole::USER);

        if ($onlyClient) {
            $results->andWhere("e.type = :client")
                ->setParameter("client", \App\Entities\User::CLIENT);
        }

		if ($onlyCompany) {
			$results->andWhere("e.type = :company")
				->setParameter("company", \App\Entities\User::COMPANY);
		}

		$results = $results->getQuery()
			->getResult();

		$data = [];*/

		/** @var \App\Entities\User $user */
		/*foreach ($results as $user) {
			$data[$user->id] = Html::el("option", [
				"data-company" => $user->isTypeCompany() ? 1 : 0,
			])->setText($user->getFullName());
		}

		return $data;
	}*/
}