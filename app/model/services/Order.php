<?php

namespace App\Services;

use App\Entities\OrderDocument;
use App\Entities\OrderItem;
use Nette\Utils\ArrayHash;

class Order extends Base {

    /**
     * @param null $managerId
     * @param null $buidlingId
     * @param null $supplierId
     * @return mixed
     */
    public function getOrders($managerId = NULL, $buidlingId = NULL, $supplierId = NULL) {

        $qb = $this->em->createQueryBuilder()
            ->select("e")
            ->from(\App\Entities\Order::class, "e");

        if ($managerId) {
            $qb->andWhere("e.manager = :manager")
                ->setParameter("manager", $managerId);
        }
        if ($buidlingId) {
            $qb->andWhere("e.building = :building")
                ->setParameter("building", $buidlingId);
        }
        if ($supplierId) {
            $qb->andWhere("e.supplier = :supplier")
                ->setParameter("supplier", $supplierId);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param ArrayHash $values
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    public function saveOrder(ArrayHash $values) {

        if ($values->id) {
            $order = $this->em->getRepository(\App\Entities\Order::class)->find($values->id);

        } else {
            $order = new \App\Entities\Order();
            $this->em->persist($order);
        }

        $order->manager = $this->em->getReference(\App\Entities\User::class, $values->manager);
        $order->supplier = $this->em->getReference(\App\Entities\Supplier::class, $values->supplier);
        $order->building = $this->em->getReference(\App\Entities\Building::class, $values->building);

        $order->state = $values->state;

        // order items
        foreach ($order->items as $item) {
            $this->em->remove($item);
        }
        $order->items->clear();

        foreach ($values->items as $item) {
            if ($item) {
                $orderItem = new OrderItem();
                $orderItem->order = $order;
                $orderItem->item = $item->item;

                $this->em->persist($orderItem);
            }
        }

        // order documents
        /*if (!empty($values->document) && $values->document->isOk() && $values->document->isImage()) {

            $orderDocument = new OrderDocument();
            $orderDocument->order = $order;
            $orderDocument->path = self::storeFile($values->document);

            $this->em->persist($orderDocument);
        }*/

        // order documents
        foreach ($values->documents as $document) {
            if (!$document->isOk() || !$document->isImage()) {
                continue;
            }

            $orderDocument = new OrderDocument();
            $orderDocument->order = $order;
            $orderDocument->path = self::storeFile($document);

            $this->em->persist($orderDocument);
        }

        $this->em->flush();

        return $order;
    }

    /**
     * @param $id
     * @return \App\Entities\Order
     */
    public function cancelledOrder($id) {

        /** @var \App\Entities\Order $order */
        $order = $this->em->getRepository(\App\Entities\Order::class)->find($id);
        $order->state = \App\Entities\Order::CANCELLED;

        $this->em->flush($order);

        return $order;
    }

    /**
     * @param $id
     * @return OrderDocument
     */
    public function deleteOrderDocument($id) {

        /** @var OrderDocument $document */
        $document = $this->em->getRepository(OrderDocument::class)->find($id);

        if ($document) {

            self::removeFile($document->path);
            $this->em->remove($document);
            $this->em->flush($document);
        }

        return $document;
    }
}