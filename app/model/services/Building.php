<?php

namespace App\Services;

use App\Entities\BuildingManager;
use Nette\Utils\ArrayHash;

class Building extends Base {

    /**
     * @param null $managerId
     * @return mixed
     */
    public function getBuildings($managerId = NULL) {

        $qb = $this->em->createQueryBuilder()
            ->select("e")
            ->from(\App\Entities\Building::class, "e")
            ->andWhere("e.deleted = 0");

        if ($managerId) {
            $qb->join("e.managers", "m")
                ->andWhere("m.manager = :manager")
                ->setParameter("manager", $managerId);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param null $managerId
     * @return array
     */
    public function getBuildingsPairs($managerId = NULL) {

        $buildings = [];

        /** @var \App\Entities\Building $building */
        foreach ($this->getBuildings($managerId) as $building) {
            $buildings[$building->id] = $building->name;
        }

        return $buildings;
    }

    /**
     * @param ArrayHash $values
     * @return \App\Entities\Building|object|null
     * @throws \Doctrine\ORM\ORMException
     */
    public function saveBuilding(ArrayHash $values) {

        if ($values->id) {
            $building = $this->em->getRepository(\App\Entities\Building::class)->find($values->id);

        } else {
            $building = new \App\Entities\Building();
            $this->em->persist($building);
        }

        /*if ($values->manager) {
            $building->manager = $this->em->getReference(\App\Entities\User::class, $values->manager);
        }*/
        $building->name = $values->name;
        $building->street = $values->street;
        $building->city = $values->city;
        $building->dateFrom = $values->dateFrom;
        $building->dateTo = $values->dateTo;
        $building->state = $values->state;

        // managers
        foreach ($building->managers as $manager) {
            $this->em->remove($manager);
        }
        $building->managers->clear();

        foreach ($values->managers as $manager) {
            if ($manager) {
                $buildingManager = new BuildingManager();
                $buildingManager->building = $building;
                $buildingManager->manager = $this->em->getReference(\App\Entities\User::class, $manager);

                $this->em->persist($buildingManager);
            }
        }

        $this->em->flush();

        return $building;
    }

    /**
     * @param $id
     * @return \App\Entities\Building
     */
    public function deleteBuilding($id) {

        /** @var \App\Entities\Building $building */
        $building = $this->em->getRepository(\App\Entities\Building::class)->find($id);
        $building->deleted = TRUE;

        // managers
        foreach ($building->managers as $manager) {
            $this->em->remove($manager);
        }
        $building->managers->clear();

        $this->em->flush($building);

        return $building;
    }

    /**
     * @param $term
     * @return array
     */
    public function autocompleteBuildings($term) {

        $results = $this->em->createQueryBuilder()
            ->select("e.name as name, e.city as city, e.street as street")
            ->from(\App\Entities\Building::class, "e")
            ->andWhere("e.name LIKE :term OR e.city LIKE :term OR e.street LIKE :term")
            ->setParameter("term", "%" . $term . "%");

        $items = [];
        foreach ($results->getQuery()->getArrayResult() as $result) {
            $items[$result["name"]] = [
                "value" => $result["name"],
                "label" => $result["name"],
            ];
        }

        return $items;
    }
}