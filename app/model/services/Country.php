<?php

namespace App\Services;

use App\Entities\WorldCountry;

class Country extends Base {

	/**
	 * @return array
	 */
	public function getWorldPairs() {

        $countryPairs = [];

        /** @var WorldCountry $countries */
		$countries = $this->em->getRepository(WorldCountry::class)->findAll();

		foreach ($countries as $country) {
            $countryPairs[$country->id] = $country->name;
        }

		asort($countryPairs);

		return $countryPairs;
	}
}