<?php

namespace App\Services;

use App\Entities\AclRolePermission;

class AclRole extends Base {

	/**
	 * @return array
	 */
	public function getPermissions() {

		$rows = $this->em->createQueryBuilder()
			->select("e")
			->from(AclRolePermission::class, "e")
			->getQuery()
			->getArrayResult();

		$items = [];
		foreach ($rows as $row) {
			foreach(\App\Entities\AclRole::getAdminRolePairs() as $role) {
				$items[$row['code']][$role] = $row[$role];
			}
		}

		return $items;
	}

}