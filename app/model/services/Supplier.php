<?php

namespace App\Services;

use Nette\Utils\ArrayHash;

class Supplier extends Base {

    /** @var \App\Security\User @inject */
    public $user;

    /**
     * @return mixed
     */
    public function getSuppliers() {

        $qb = $this->em->createQueryBuilder()
            ->select("e")
            ->from(\App\Entities\Supplier::class, "e")
            ->andWhere("e.deleted = 0");

        return $qb->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function getSuppliersPairs() {
        $suppliers = [];

        /** @var \App\Entities\Supplier $supplier */
        foreach ($this->getSuppliers() as $supplier) {
            $suppliers[$supplier->id] = $supplier->company;
        }

        return $suppliers;
    }

    /**
     * @param ArrayHash $values
     */
    public function saveSupplier(ArrayHash $values) {

        if ($values->id) {
            /** @var \App\Entities\Supplier $supplier */
            $supplier = $this->em->getRepository(\App\Entities\Supplier::class)->find($values->id);

        } else {
            $supplier = new \App\Entities\Supplier();
            $this->em->persist($supplier);
        }

        $supplier->company = $values->company;
        $supplier->email = $values->email;
        $supplier->phone = $values->phone;
        $supplier->street = $values->street;
        $supplier->city = $values->city;
        $supplier->zip = $values->zip;
        $supplier->ino = $values->ino;
        $supplier->vat = $values->vat;

        $this->em->flush($supplier);
    }

    /**
     * @param $id
     * @return \App\Entities\Supplier
     */
    public function deleteSupplier($id) {

        /** @var \App\Entities\Supplier $supplier */
        $supplier = $this->em->getRepository(\App\Entities\Supplier::class)->find($id);
        $supplier->deleted = TRUE;

        $this->em->flush($supplier);

        return $supplier;
    }

}