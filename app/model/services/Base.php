<?php

namespace App\Services;

use Nettrine\ORM\EntityManagerDecorator as EntityManager;
use Nette\Http\FileUpload;
use Nette\Utils\Image;

class Base {

	/** @var EntityManager @inject */
	public $em;

    /**
     * Zpracování uploadu
     *
     * @param $image
     * @param string $extension
     * @return string
     * @throws \Nette\Utils\ImageException
     */
	public static function storePhoto($image, $extension = "jpg") {

		if ($image instanceof FileUpload) {
			$extension = explode(".", $image->getSanitizedName());
			$extension = end($extension);

			/** @var \Nette\Utils\Image $image */
			$image = $image->toImage();
		}

		$fileName = uniqid() . "." . $extension;
		$directory = self::getUploadDirectory();

		$image->save(UPLOAD_PATH . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR. $fileName, 70,Image::JPEG);

		return UPLOAD_DIR . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR. $fileName;
	}

	/**
	 * @param FileUpload $file
	 * @return string
	 */
	public static function storeFile(FileUpload $file) {
		$extension = explode(".", $file->getSanitizedName());
		$extension = end($extension);

		$fileName = uniqid() . "." . $extension;
		$directory = self::getUploadDirectory();

		$file->move(UPLOAD_PATH . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR. $fileName);

		return UPLOAD_DIR . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR. $fileName;
	}

    /**
     * @return int
     */
	protected static function getUploadDirectory() {
		$directory = rand(11, 99);
		$path = UPLOAD_PATH. DIRECTORY_SEPARATOR . $directory;

		if (!is_dir($path)) {
			mkdir($path, 0777, TRUE);
		}

		return $directory;
	}

    /**
     * @param $path
     * @return bool
     */
	public static function removeFile($path) {
		return @unlink(UPLOAD_PATH. DIRECTORY_SEPARATOR . $path);
	}

}