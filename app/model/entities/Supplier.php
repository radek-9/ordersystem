<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="supplier")
 */
class Supplier {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=32, nullable=false)
     */
    public $company;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    public $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=false)
     */
    public $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=false)
     */
    public $street;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    public $city;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=255, nullable=false)
     */
    public $zip;

    /**
     * IČO
     *
     * @var string
     * @ORM\Column(name="ino", type="string", length=255, nullable=false)
     */
    public $ino;

    /**
     * DIČ
     *
     * @var string
     * @ORM\Column(name="vat", type="string", length=255, nullable=false)
     */
    public $vat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false, options={"default":0}))
     */
    public $deleted = FALSE;

}