<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="acl_role")
 */
class AclRole {

	const ADMIN = 1;
	const MANAGER = 2;
    const CUSTOMER = 3;

    /**
     * @return array
     */
    public static function getRolePairs() {
        return [
            self::ADMIN => 'admin',
            self::MANAGER => 'manager',
            self::CUSTOMER => 'customer',
        ];
    }

    /**
     * @return array
     */
    public static function getAdminRolePairs() {
        return [
            self::ADMIN => 'admin',
            self::MANAGER => 'manager',
        ];
    }

    /**
     * @return array
     */
    public static function getCustomerRolePairs() {
        return [
            self::CUSTOMER => 'customer',
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getRoleName($id) {
        return self::getRolePairs()[$id];
    }

	/**
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue
	*/
	public $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="code", type="string", length=32, nullable=false)
	 */
	public $code;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="is_admin", type="boolean", nullable=false, options={"default": 0})
	 */
	public $isAdmin;
}