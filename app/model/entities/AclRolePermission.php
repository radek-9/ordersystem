<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="acl_role_permission")
 */
class AclRolePermission {

	/**
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue
	*/
	public $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="code", type="string", length=255, nullable=false)
	 */
	public $code;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="admin", type="integer", length=11, nullable=false, options={"default":0}))
	 */
	public $admin = 0;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="manager", type="integer", length=11, nullable=false, options={"default":0}))
	 */
	public $manager = 0;

}