<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;
use Nette\Security\IIdentity;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User implements IIdentity {

	const CLIENT = 1;
	const COMPANY = 2;

	const SALUTATION_MALE = 1;
	const SALUTATION_FEMALE = 2;

	const BILLING_ONLINE = 1;
	const BILLING_INVOICE = 2;

	// heslo pro hash
	const HASH = "ABC159@";

    /**
     * @return array
     */
    public static function getTypePairs() {
        return [
            self::CLIENT => 'Koncový klient',
            self::COMPANY => 'Firma',
        ];
    }

    /**
     * @return array
     */
    public static function getSalutationPairs() {
        return [
            self::SALUTATION_MALE => 'Muž',
            self::SALUTATION_FEMALE => 'Žena',
        ];
    }

    /**
     * @return array
     */
    public static function getBillingPairs() {
        return [
            self::BILLING_ONLINE => 'Online',
            self::BILLING_INVOICE => 'Na fakturu',
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getTypeName($id) {
        return self::getTypePairs()[$id];
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getSalutationName($id) {
        return self::getSalutationPairs()[$id];
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getBillingName($id) {
        return self::getBillingPairs()[$id];
    }

	/**
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue
	*/
	public $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="type", type="integer", length=1, nullable=false, options={"default":1}))
	 */
	public $type = self::CLIENT;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="billing", type="integer", length=1, nullable=true, options={"default":1}))
	 */
	public $billing = self::BILLING_ONLINE;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
	 */
	public $firstName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
	 */
	public $lastName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=255, nullable=false)
	 */
	public $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="phone", type="string", length=255, nullable=true)
	 */
	public $phone;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="street", type="string", length=255, nullable=true)
	 */
	public $street;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="city", type="string", length=255, nullable=true)
	 */
	public $city;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="zip", type="string", length=255, nullable=true)
	 */
	public $zip;

	/**
	 * @var \App\Entities\WorldCountry
	 *
	 * @ORM\ManyToOne(targetEntity="WorldCountry")
	 * @ORM\JoinColumn(name="worldCountry_id", referencedColumnName="id")
	 */
	public $country;

	/**
	 * @var string
	 * @ORM\Column(name="password", type="string", length=60, nullable=true)
	 */
	public $password;

	/**
	 * @var string
	 * @ORM\Column(name="loginSalt", type="string", length=60, nullable=true)
	 */
	public $loginSalt;

	/**
	 * @var string
	 * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
	 */
	public $avatar;

	/**
	 * @var \App\Entities\AclRole
	 *
	 * @ORM\ManyToOne(targetEntity="AclRole")
	 * @ORM\JoinColumn(name="aclRole_id", referencedColumnName="id")
	 */
	public $aclRole;

    /**
     * @var string
     * @ORM\Column(name="fbUserId", type="string", length=255, nullable=true)
     */
    public $fbUserId;

    /**
     * @var string
     * @ORM\Column(name="fbAccessToken", type="string", length=255, nullable=true)
     */
    public $fbAccessToken;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="active", type="boolean", nullable=false, options={"default":0}))
	 */
	public $active = FALSE;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="deleted", type="boolean", nullable=false, options={"default":0}))
	 */
	public $deleted = FALSE;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="salutation", type="integer", length=1, nullable=false, options={"default":1}))
	 */
	public $salutation = self::SALUTATION_MALE;

	/**
	 * firma
	 *
	 * @var string
	 * @ORM\Column(name="company", type="string", length=255, nullable=true)
	 */
	public $company;

	/**
	 * IČO
	 *
	 * @var string
	 * @ORM\Column(name="ino", type="string", length=255, nullable=true)
	 */
	public $ino;

	/**
	 * DIČ
	 *
	 * @var string
	 * @ORM\Column(name="vat", type="string", length=255, nullable=true)
	 */
	public $vat;

    /**
     * @var string
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    public $logo;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="note", type="text", nullable=true)
	 */
	public $note;


	public function getId() {
		return $this->id;
	}

	public function getRoles():array {
		return [];
	}

    /**
     * Nastaví nové heslo, po registraci
     *
     * @param $password
     */
	public function setNewPassword($password) {
		$this->loginSalt = \Nette\Utils\Random::generate(16);
		$this->password = $this->calculateHash($password, $this->loginSalt);
	}

    /**
     * Změní uživateli heslo
     *
     * @param $password
     * @param bool $oldPassword
     */
	public function changePassword($password, $oldPassword = FALSE) {
		if ($oldPassword !== FALSE) {
			if (!$this->verifyPassword($oldPassword))
				throw new InvalidPasswordException("password.mismatch");
		}

		$this->loginSalt = base64_encode(\Nette\Utils\Random::generate(16));
		$this->password = $this->calculateHash($password, $this->loginSalt);
	}

	/**
	 * @param $password
	 * @return bool
	 */
	public function verifyPassword($password) {
		return $this->password === $this->calculateHash($password, $this->loginSalt);
	}

	/**
	 * @param string $password
	 * @param string $salt
	 * @return string
	 */
	public function calculateHash($password, $salt) {
		return base64_encode(hash_hmac('sha256', base64_decode($salt) . iconv('UTF-8', 'UTF-16LE', $password), $salt, true));
	}

	/**
	 * @return string
	 */
	public function getFullName() {
		if ($this->type === self::COMPANY) {
			return $this->company;
		}

		return $this->firstName . " " . $this->lastName;
	}

    /**
     * @return string
     */
	public function getProfileName() {
		if ($this->type === self::COMPANY) {
			return $this->company;
		}

		return $this->lastName;
	}

	/**
	 * @return string
	 */
	public function getAvatarPath() {
		return $this->avatar ?: "res/avatar.png";
	}

    /**
     * @return string
     */
    public function getLogoPath() {
        return $this->logo ?: "res/logo.png";
    }

	/**
	 * @return bool
	 */
	public function isTypeClient() {
		return $this->type === self::CLIENT;
	}

	/**
	 * @return bool
	 */
	public function isTypeCompany() {
		return $this->type === self::COMPANY;
	}

    /**
     * @return bool
     */
    public function isAdmin() {
        return FALSE;
    }

    /**
     * @return bool
     */
    public function isManager() {
        return $this->aclRole->id === AclRole::MANAGER;
    }

    /**
     * @return bool
     */
    public function isCustomer() {
        return $this->aclRole->id === AclRole::CUSTOMER;
    }

    /**
     * @return bool
     */
    public function isMale() {
        return $this->salutation === self::SALUTATION_MALE;
    }

    /**
     * @return bool
     */
    public function isFemale() {
        return $this->salutation === self::SALUTATION_FEMALE;
    }

	/**
	 * @return string
	 */
	public function getFullAddress() {
		$address = [
			$this->street,
			$this->city,
			$this->zip,
		];

		return implode(", ", array_filter($address));
	}
}