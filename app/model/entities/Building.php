<?php

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="building")
 */
class Building {

    const NEW = 1;
    const IN_PROGRESS = 2;
    const COMPLETED = 3;
    const CANCELLED = 4;

    /**
     * @return array
     */
    public static function getStatePairs() {
        return [
            self::NEW => 'new',
            self::IN_PROGRESS => 'in progress',
            self::COMPLETED => 'completed',
            self::CANCELLED => 'cancelled',
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getStateName($id) {
        return self::getStatePairs()[$id];
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32, nullable=false)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    public $street;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    public $city;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFrom", type="datetime", nullable=false)
     */
    public $dateFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateTo", type="datetime", nullable=false)
     */
    public $dateTo;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer", length=1, nullable=false, options={"default":1}))
     */
    public $state = self::NEW;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false, options={"default":0}))
     */
    public $deleted = FALSE;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="BuildingManager", mappedBy="building")
     */
    public $managers;

    public function __construct() {
        $this->managers = new ArrayCollection;
    }

    /**
     * @return bool
     */
    public function isNew() {
        return $this->state === self::NEW;
    }

    /**
     * @return bool
     */
    public function isInProgress() {
        return $this->state === self::IN_PROGRESS;
    }

    /**
     * @return bool
     */
    public function isCompleted() {
        return $this->state === self::COMPLETED;
    }

    /**
     * @return bool
     */
    public function isCancelled() {
        return $this->state === self::COMPLETED;
    }
}