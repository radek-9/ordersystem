<?php

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="`order`")
 */
class Order {

    const IN_PROGRESS = 1;
    const SHIPPED = 2;
    const CANCELLED = 3;

    /**
     * @return array
     */
    public static function getStatePairs() {
        return [
            self::IN_PROGRESS => 'in progress',
            self::SHIPPED => 'shipped',
            self::CANCELLED => 'cancelled',
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getStateName($id) {
        return self::getStatePairs()[$id];
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @var \App\Entities\User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    public $manager;

    /**
     * @var \App\Entities\Supplier
     *
     * @ORM\ManyToOne(targetEntity="Supplier")
     * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id")
     */
    public $supplier;

    /**
     * @var \App\Entities\Building
     *
     * @ORM\ManyToOne(targetEntity="Building")
     * @ORM\JoinColumn(name="building_id", referencedColumnName="id")
     */
    public $building;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer", length=1, nullable=false, options={"default":1}))
     */
    public $state = self::IN_PROGRESS;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    public $created;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="OrderItem", mappedBy="order")
     */
    public $items;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="OrderDocument", mappedBy="order")
     */
    public $documents;

    /**
     * Order constructor.
     * @throws \Exception
     */
    public function __construct() {
        $this->created = new \DateTime;
        $this->items = new ArrayCollection;
        $this->documents = new ArrayCollection;
    }

    /**
     * @return bool
     */
    public function isInProgress() {
        return $this->state === self::IN_PROGRESS;
    }

    /**
     * @return bool
     */
    public function isShipped() {
        return $this->state === self::SHIPPED;
    }

    /**
     * @return bool
     */
    public function isCancelled() {
        return $this->state === self::CANCELLED;
    }
}