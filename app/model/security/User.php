<?php

namespace App\Security;

use App\Entities\AclRole;

class User extends \Nette\Security\User {

	/** @var \App\Services\AclRole @inject */
	public $aclRoleService;

	/** @var array */
	protected $permissions = [];

	public function isAdmin() {
		return $this->identity->aclRole->id === AclRole::ADMIN;
	}

	public function isManager() {
		return $this->identity->aclRole->id === AclRole::MANAGER;
	}

    public function isCustomer() {
        return $this->identity->aclRole->id === AclRole::CUSTOMER;
    }

    /**
     * @param $permission
     * @return mixed
     */
    public function allowed($permission) {

        if (!$this->permissions) {
            $this->permissions = $this->aclRoleService->getPermissions();
        }

        return $this->permissions[$permission][$this->identity->aclRole->code];
    }
}