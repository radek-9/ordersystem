<?php

namespace App\Components;

use Nette;
use Ublaboo\DataGrid\Exception\DataGridException;

class DataGrid extends \Ublaboo\DataGrid\DataGrid
{

	public function __construct($parent = null, $name = null)
	{
		parent::__construct($parent, $name);

		$this->setTemplateFile(__DIR__ . "/templates/datagrid.latte");
		$this->setRememberState(FALSE);
		$this->strictSessionFilterValues = FALSE;
	}

	/**
	 * Get associative array of items_per_page_list
	 * @return array
	 */
	public function getItemsPerPageList():array
	{
		if (empty($this->itemsPerPageList)) {
			$this->setItemsPerPageList([50, 100, 500], FALSE);
		}

		$list = array_flip($this->itemsPerPageList);

		foreach ($list as $key => $value) {
			$list[$key] = $key;
		}

		if (array_key_exists('all', $list)) {
			$list['all'] = 'Všechny';
		}

		return $list;
	}

    /**
     * @return \Ublaboo\DataGrid\Filter\SubmitButton
     * @throws DataGridException
     */
	public function getFilterSubmitButton(): \Ublaboo\DataGrid\Filter\SubmitButton
	{
		if ($this->hasAutoSubmit()) {
			throw new DataGridException(
				'DataGrid has auto-submit. Turn it off before setting filter submit button.'
			);
		}

		if ($this->filterSubmitButton === null) {
			$this->filterSubmitButton = new \Ublaboo\DataGrid\Filter\SubmitButton($this);
			$this->filterSubmitButton->setText("NASTAVIT FILTR");
			$this->filterSubmitButton->setHtmlId("grid_submit");
		}

		return $this->filterSubmitButton;
	}

	/**
	 * @return array
	 */
	public function getActiveFilter() {
		$data = [];
		$activeFilter = [];

		foreach ($this->filters as $column => $columnFilter) {
			if ($filter = $this->getSessionData($column)) {
				$data[$columnFilter->getName()] = $filter;
				$activeFilter[$columnFilter->getKey()] = $filter;
			}
		}

		$this["filter"]["filter"]->setDefaults($activeFilter);

		return array_filter($data);
	}
}