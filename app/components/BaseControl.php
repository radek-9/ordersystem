<?php

namespace App\Components;

use App\Security\User;
use Nettrine\ORM\EntityManagerDecorator as EntityManager;

class BaseControl extends \Nette\Application\UI\Control {

	use \Kdyby\Autowired\AutowireProperties;
	use \Kdyby\Autowired\AutowireComponentFactories;

	/** @var User @autowire */
	protected $user;

	/** @var EntityManager @autowire */
	protected $em;

	/** @var string */
	protected $templateFilename;

	public function render() {
		$this->template->setFile(
			(!empty($this->templateFilename)) ? $this->templateFilename : str_replace('.php', '.latte', $this->getReflection()->getFileName())
		);

		$this->template->render();
	}

	public function isAjax() {
		return $this->presenter->isAjax();
	}

	/**
	 * @param \Nette\Forms\Controls\SubmitButton $button
	 */
	public function addRow(\Nette\Forms\Controls\SubmitButton $button) {
		$button->parent->createOne();
	}

	/**
	 * @param \Nette\Forms\Controls\SubmitButton $button
	 */
	public function removeRow(\Nette\Forms\Controls\SubmitButton $button) {
		$button->parent->parent->remove($button->parent, TRUE);
	}

	/**
	 * @param \Kdyby\Doctrine\QueryBuilder $qb
	 * @param $params
	 */
	/*public function baseSortCallback(\Kdyby\Doctrine\QueryBuilder $qb, $params) {
		foreach ($params as $column => $value) {
			$qb->orderBy("e.$column", $value);
		}
	}*/

	/**
	 * @param $message
	 * @param string $type
	 * @return \stdClass
	 */
	public function flashMessage($message, $type = 'info'):\stdClass {
		return parent::flashMessage($message, $type);
	}
}
