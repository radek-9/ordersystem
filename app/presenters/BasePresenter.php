<?php

namespace App\Presenters;

use App\Services\Building;
use Nettrine\ORM\EntityManagerDecorator as EntityManager;
use Kdyby\Autowired\AutowireComponentFactories;
use Kdyby\Autowired\AutowireProperties;
use Nette\Application\UI\Presenter;
use Nette\Utils\Strings;

class BasePresenter extends Presenter {

	use AutowireProperties;
	use AutowireComponentFactories;

	/** @var EntityManager @autowire */
	protected $em;

    /** @var Building @autowire */
    protected $buildingService;

    /**
     * @throws \Nette\Application\AbortException
     */
	protected function startup() {
		parent::startup();

		// zablokovaný účet
		if ($this->user->isLoggedIn() && $this->user->identity->deleted) {
			$this->user->logout(TRUE);
			$this->redirect("Sign:in");
		}

	}

	/**
	 * @param $message
	 * @param string $type
	 * @return \stdClass
	 */
	public function flashMessage($message, $type = 'info'):\stdClass {
		return parent::flashMessage($message, $type);
	}

    /**
     * @param $term
     * @throws \Nette\Application\AbortException
     */
    public function handleBuildingAutocompleteRequest($term) {
        $items = $this->buildingService->autocompleteBuildings($term);

        $this->sendJson(array_values($items));
    }
}